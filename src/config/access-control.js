module.exports = {
    permissions: ["USER_FULL", "ADMIN_FULL"],
    roles: [
        {
            role: "USER",
            permissions: ["USER_FULL"]
        },
        {
            role: "ADMIN",
            permissions: ["ADMIN_FULL"]
        }
    ]
};
