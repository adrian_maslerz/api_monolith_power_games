//loading environment config
const env = process.env;

module.exports = {
	app: {
		name: env.APP_NAME,
		app_domain: env.APP_DOMAIN,
		admin_domain: env.ADMIN_DOMAIN,
		contact_mail: env.CONTACT_EMAIL
	},
	database: {
		host: env.DB_HOST || "localhost",
		srv: Boolean(parseInt(env.DB_SRV)),
		port: env.DB_PORT,
		user: env.DB_USER || "",
		password: env.DB_PASSWORD || "",
		database: env.DB_DATABASE
	},
	services: {
		stripe: {
			key: env.STRIPE_KEY
		}
	},
	mailer: {
		from: env.MAIL_FROM + "<" + env.MAIL_FROM_ADDRESS + ">",
		sendgrid: {
			key: env.SENDGRID_API_KEY,
			templates: {
				"forgot-password": env.SENDGRID_TEMPLATE_FORGOT_PASSWORD,
				"account-created": env.SENDGRID_TEMPLATE_ACCOUNT_CREATED,
				"order-placed": env.SENDGRID_TEMPLATE_ORDER_PLACED,
				"game-codes": env.SENDGRID_TEMPLATE_GAME_CODES,
				"contact-message": env.SENDGRID_TEMPLATE_CONTACT_MESSAGE,
			}
		}
	},
	folders: {
		views: __dirname + "/../views",
		uploads: __dirname + "/../public/uploads",
		public: __dirname + "/../public",
	},
	paths: {
		folders: {
			uploads: "/uploads"
		},
		emails: {
			PASSWORD_RESET: "/password-reset?token=:token"
		}
	}
}
