module.exports = function (grunt)
{
	require('load-grunt-tasks')(grunt);
	grunt.initConfig({

		pkg: grunt.file.readJSON("package.json"),
		concurrent: {
			docs: {
				tasks: [ 'watch:docs_api' ],
				options: {
					logConcurrentOutput: true
				}
			}
		},
		watch: {
			docs_api: {
				files: 'docs/api/input/*.js',
				tasks: [ 'apidoc:api' ]
			}
		},
		apidoc: {
			api: {
				src: "docs/api/input/",
				dest: "public/v1/",
				template: "docs/template/",
			}
		}
	});
	grunt.registerTask('docs', [ 'concurrent:docs' ]);
};
