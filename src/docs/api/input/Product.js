/**
 * @api {post} /products Add product
 * @apiSampleRequest /products
 * @apiVersion 0.0.1
 * @apiName Add product
 * @apiGroup Product
 *
 * @apiParam {String} title Product title. Max 100 characters.
 * @apiParam {String} category Product category.
 * @apiParam {File} image Product image file. Max file size 10MB. Allowed mime types: image/jpg, image/jpeg, image/png
 * @apiParam {String} manufacturer Product manufacturer. Max 100 characters.
 * @apiParam {Number} availability Product availability. Integer value. Min 0.
 * @apiParam {String} intro Product intro.
 * @apiParam {String} description Product description.
 * @apiParam {Number} price Product price. Float value. Min 0.
 * @apiParam {String="7", "12", "16", "18", "No information"} age_category Product age category.
 * @apiParam {String="Polish", "English", "German", "French", "Spanish", "Italian", "Japanese"} languages Product languages encoded as string array, eg. ["Polish", "English"]
 * @apiParam {String="Single player", "Multiplayer", "Massive online multiplayer"} game_modes Product game modes encoded as string array, eg. ["Multiplayer", "Massive online multiplayer"]
 * @apiParam {String="Basic", "DLC addition", "GOTY edition", "VR"} release_type Product release type.
 * @apiParam {Number} premiere Product premiere date as timestamp ms.
 *
 * @apiSuccess (Success 201){Boolean} status Operation status.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "status": true
 *     }
 *
 * @apiUse WrongParametersError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "title",
 *                   "message": "Title is required"
 *              }
 *          ]
 *      }
 *
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 */

/**
 * @api {get} /products Products list
 * @apiSampleRequest /products
 * @apiVersion 0.0.1
 * @apiName Products list
 * @apiGroup Product
 *
 * @apiUse PaginationElements

 * @apiParam {String} [search] Search string.
 * @apiParam {String} [category] Category id.
 * @apiParam {Number} [premiere_from] Premiere from.
 * @apiParam {Number} [premiere_to] Premiere to.
 * @apiParam {Number} [price_from] Price from.
 * @apiParam {Number} [price_to] Price to.
 * @apiParam {String="7", "12", "16", "18", "No information"} age_categories Product age categories encoded as string array, eg. ["16", "18"]
 * @apiParam {String="Polish", "English", "German", "French", "Spanish", "Italian", "Japanese"} languages Product languages encoded as string array, eg. ["Polish", "English"]
 * @apiParam {String="Single player", "Multiplayer", "Massive online multiplayer"} game_modes Product game modes encoded as string array, eg. ["Multiplayer", "Massive online multiplayer"]
 * @apiParam {String="Basic", "DLC addition", "GOTY edition", "VR"} release_types Product release types encoded as string array, eg. ["Basic", "VR"]
 * @apiParam {String="createdASC", "createdDESC", "titleASC", "titleDESC", "manufacturerASC", "manufacturerDESC", "priceASC", "priceDESC", "premiereASC", "premiereDESC", "availabilityASC", "availabilityDESC", "rate", "rateDESC"} [sort="createdDESC"] Sort option.
 *
 * @apiSuccess {Object[]} results Products list.
 *
 * @apiSuccess {String} results._id Product id
 * @apiSuccess {String} results.title Product title
 * @apiSuccess {String} results.image Product image
 * @apiSuccess {String} results.manufacturer Product manufacturer
 * @apiSuccess {Number} results.availability Product availability
 * @apiSuccess {String} results.intro Product intro
 * @apiSuccess {Number} results.price Product price
 * @apiSuccess {Number} results.rate Product rate
 * @apiSuccess {Number} results.premiere Product premiere date as timestamp ms.
 * @apiSuccess {Number} results.created Product created date as timestamp ms.
 *
 * @apiSuccess {Object} results.category Category object
 * @apiSuccess {String} results.category._id Category id
 * @apiSuccess {String} results.category.title Category title
 * @apiSuccess {Number} results.category.created Category created date as timestamp ms.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "results": [
 *              {
 *                  "_id": "5f0af39a1c5d9d0049be2e22",
 *                  "created": 1594553242264,
 *                  "title": "BATTLEFRONT II PS5",
 *                  "category": {
 *                      "_id": "5f09de8454e0c4003b5e4189",
 *                      "created": 1594482308090,
 *                      "title": "PC"
 *                  },
 *                  "image": "localhost:8000/products/uploads/products/5f0af39a1c5d9d0049be2e22/siBownia.jpg",
 *                  "manufacturer": "EA",
 *                  "availability": 10,
 *                  "intro": "test teste test",
 *                  "price": 249.99,
 *                  "rate": 5,
 *                  "premiere": 1573776000000
 *              },
 *          ],
 *          "pages": 1,
 *          "total": 3,
 *     }
 **/

/**
 * @api {get} /products/:id Product details
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Product details
 * @apiGroup Product
 *
 * @apiParam {String} id Product id.
 *
 * @apiSuccess {String} _id Product id
 * @apiSuccess {String} title Product title
 * @apiSuccess {String} image Product image
 * @apiSuccess {String} manufacturer Product manufacturer
 * @apiSuccess {Number} availability Product availability
 * @apiSuccess {String} intro Product intro
 * @apiSuccess {String} description Product description
 * @apiSuccess {Number} price Product price
 * @apiSuccess {String[]="Polish", "English", "German", "French", "Spanish", "Italian", "Japanese"} languages Product languages
 * @apiSuccess {String[]="Single player", "Multiplayer", "Massive online multiplayer"} game_modes Product game modes
 * @apiSuccess {String="Single player", "Multiplayer", "Massive online multiplayer"} release_type Product release type
 * @apiSuccess {String="7", "12", "16", "18", "No information"} age_category Product age category
 * @apiSuccess {Number} rate Product rate.
 * @apiSuccess {Number} premiere Product premiere date as timestamp ms.
 * @apiSuccess {Number} created Product created date as timestamp ms.
 *
 * @apiSuccess {Object} category Category object
 * @apiSuccess {String} category._id Category id
 * @apiSuccess {String} category.title Category title
 * @apiSuccess {Number} category.created Category created date as timestamp ms.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *          "_id": "5f0af39a1c5d9d0049be2e22",
 *          "languages": [
 *              "Polish",
 *              "English"
 *          ],
 *          "game_modes": [
 *              "Single player",
 *              "Multiplayer"
 *          ],
 *          "created": 1594553242264,
 *          "title": "BATTLEFRONT II PS5",
 *          "category": {
 *              "_id": "5f09de8454e0c4003b5e4189",
 *              "created": 1594482308090,
 *              "title": "PC"
 *          },
 *          "image": "localhost:8000/products/uploads/products/5f0af39a1c5d9d0049be2e22/siBownia.jpg",
 *          "manufacturer": "EA",
 *          "availability": 10,
 *          "intro": "test teste test",
 *          "description": "asda",
 *          "price": 249.99,
 *          "rate": 5,
 *          "age_category": "12",
 *          "release_type": "Basic",
 *          "premiere": 1573776000000
 *      }
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "product",
 *                   "message": "Product not found"
 *              }
 *          ]
 *      }
 **/

/**
 * @api {put} /products/:id Update product
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Update product
 * @apiGroup Product
 *
 * @apiParam {String} id Product id.
 *
 * @apiParam {String} [title] Product title. Max 100 characters.
 * @apiParam {String} [category] Product category.
 * @apiParam {File} [image] Product image file. Max file size 10MB. Allowed mime types: image/jpg, image/jpeg, image/png
 * @apiParam {String} [manufacturer] Product manufacturer. Max 100 characters.
 * @apiParam {Number} [availability] Product availability. Integer value. Min 0.
 * @apiParam {String} [intro] Product intro.
 * @apiParam {String} [description] Product description.
 * @apiParam {Number} [price] Product price. Float value. Min 0.
 * @apiParam {String="7", "12", "16", "18", "No information"} [age_category] Product age category.
 * @apiParam {String="Polish", "English", "German", "French", "Spanish", "Italian", "Japanese"} [languages] Product languages encoded as string array, eg. ["Polish", "English"]
 * @apiParam {String="Single player", "Multiplayer", "Massive online multiplayer"} [game_modes] Product game modes encoded as string array, eg. ["Multiplayer", "Massive online multiplayer"]
 * @apiParam {String="Basic", "DLC addition", "GOTY edition", "VR"} [release_type] Product release type.
 * @apiParam {Number} [premiere] Product premiere date as timestamp ms.
 *
 * @apiUse SuccessOperationStatus
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "product",
 *                   "message": "Product not found"
 *              }
 *          ]
 *      }
 * @apiUse WrongParametersError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "title",
 *                   "message": "Title is required"
 *              }
 *          ]
 *      }
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 **/

/**
 * @api {delete} /products/:id Delete product
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Delete product
 * @apiGroup Product
 *
 * @apiParam {String} id Product id.
 *
 * @apiUse SuccessOperationStatus
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "product",
 *                   "message": "Product not found"
 *              }
 *          ]
 *      }
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 **/

/**
 * @api {post} /products/photos Upload photo
 * @apiSampleRequest /products/photos
 * @apiVersion 0.0.1
 * @apiName Upload photo
 * @apiGroup Product
 *
 * @apiParam { File } photo Photo file. Available mime types: image/png, image/jpeg. Max file size 5MB.
 *
 * @apiSuccess (Success 201){Boolean} status Operation status.
 * @apiSuccess (Success 201){String} originalName Original name.
 * @apiSuccess (Success 201){String} generatedName Generated name.
 * @apiSuccess (Success 201){String} msg Success message.
 * @apiSuccess (Success 201){String} imageUrl Image url.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "status": true,
 *         "originalName": "IMG_0437.jpeg",
 *         "generatedName": "1575644936824IMG_0437.jpeg",
 *         "msg": "Image upload successful",
 *         "imageUrl": "http://localhost:8000/uploads/products/photos/1575644936824IMG_0437.jpeg"
 *     }
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 */

/**
 * @api {get} /products/home Products home
 * @apiSampleRequest /products/home
 * @apiVersion 0.0.1
 * @apiName Products home
 * @apiGroup Product
 *
 * @apiSuccess {Object[]} newest Products list.
 *
 * @apiSuccess {String} newest._id Product id
 * @apiSuccess {String} newest.title Product title
 * @apiSuccess {String} newest.image Product image
 * @apiSuccess {String} newest.manufacturer Product manufacturer
 * @apiSuccess {Number} newest.availability Product availability
 * @apiSuccess {String} newest.intro Product intro
 * @apiSuccess {Number} newest.price Product price
 * @apiSuccess {Number} newest.rate Product rate
 * @apiSuccess {Number} newest.premiere Product premiere date as timestamp ms.
 * @apiSuccess {Number} newest.created Product created date as timestamp ms.
 *
 * @apiSuccess {Object} newest.category Category object
 * @apiSuccess {String} newest.category._id Category id
 * @apiSuccess {String} newest.category.title Category title
 * @apiSuccess {Number} newest.category.created Category created date as timestamp ms.
 *
 * @apiSuccess {Object[]} premieres Products list.
 *
 * @apiSuccess {String} premieres._id Product id
 * @apiSuccess {String} premieres.title Product title
 * @apiSuccess {String} premieres.image Product image
 * @apiSuccess {String} premieres.manufacturer Product manufacturer
 * @apiSuccess {Number} premieres.availability Product availability
 * @apiSuccess {String} premieres.intro Product intro
 * @apiSuccess {Number} premieres.price Product price
 * @apiSuccess {Number} premieres.rate Product rate
 * @apiSuccess {Number} premieres.premiere Product premiere date as timestamp ms.
 * @apiSuccess {Number} premieres.created Product created date as timestamp ms.
 *
 * @apiSuccess {Object} premieres.category Category object
 * @apiSuccess {String} premieres.category._id Category id
 * @apiSuccess {String} premieres.category.title Category title
 * @apiSuccess {Number} premieres.category.created Category created date as timestamp ms.
 *
 * @apiSuccess {Object[]} recommended Products list.
 *
 * @apiSuccess {String} recommended._id Product id
 * @apiSuccess {String} recommended.title Product title
 * @apiSuccess {String} recommended.image Product image
 * @apiSuccess {String} recommended.manufacturer Product manufacturer
 * @apiSuccess {Number} recommended.availability Product availability
 * @apiSuccess {String} recommended.intro Product intro
 * @apiSuccess {Number} recommended.price Product price
 * @apiSuccess {Number} recommended.rate Product rate
 * @apiSuccess {Number} recommended.premiere Product premiere date as timestamp ms.
 * @apiSuccess {Number} recommended.created Product created date as timestamp ms.
 *
 * @apiSuccess {Object} recommended.category Category object
 * @apiSuccess {String} recommended.category._id Category id
 * @apiSuccess {String} recommended.category.title Category title
 * @apiSuccess {Number} recommended.category.created Category created date as timestamp ms.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "newest": [
 *              {
 *                  "_id": "5f0af39a1c5d9d0049be2e22",
 *                  "created": 1594553242264,
 *                  "title": "BATTLEFRONT II PS5",
 *                  "category": {
 *                      "_id": "5f09de8454e0c4003b5e4189",
 *                      "created": 1594482308090,
 *                      "title": "PC"
 *                  },
 *                  "image": "localhost:8000/products/uploads/products/5f0af39a1c5d9d0049be2e22/siBownia.jpg",
 *                  "manufacturer": "EA",
 *                  "availability": 10,
 *                  "intro": "test teste test",
 *                  "price": 249.99,
 *                  "rate": 5,
 *                  "premiere": 1573776000000
 *              },
 *          ],
 *          "premieres": [
 *              {
 *                  "_id": "5f0af39a1c5d9d0049be2e22",
 *                  "created": 1594553242264,
 *                  "title": "BATTLEFRONT II PS5",
 *                  "category": {
 *                      "_id": "5f09de8454e0c4003b5e4189",
 *                      "created": 1594482308090,
 *                      "title": "PC"
 *                  },
 *                  "image": "localhost:8000/products/uploads/products/5f0af39a1c5d9d0049be2e22/siBownia.jpg",
 *                  "manufacturer": "EA",
 *                  "availability": 10,
 *                  "intro": "test teste test",
 *                  "price": 249.99,
 *                  "rate": 5,
 *                  "premiere": 1573776000000
 *              },
 *          ],
 *          "recommended": [
 *              {
 *                  "_id": "5f0af39a1c5d9d0049be2e22",
 *                  "created": 1594553242264,
 *                  "title": "BATTLEFRONT II PS5",
 *                  "category": {
 *                      "_id": "5f09de8454e0c4003b5e4189",
 *                      "created": 1594482308090,
 *                      "title": "PC"
 *                  },
 *                  "image": "localhost:8000/products/uploads/products/5f0af39a1c5d9d0049be2e22/siBownia.jpg",
 *                  "manufacturer": "EA",
 *                  "availability": 10,
 *                  "intro": "test teste test",
 *                  "price": 249.99,
 *                  "rate": 5,
 *                  "premiere": 1573776000000
 *              },
 *          ],
 *     }
 **/
