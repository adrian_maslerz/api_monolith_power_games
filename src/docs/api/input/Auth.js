
/**
 * @api {post} /register Register
 * @apiSampleRequest /register
 * @apiVersion 0.0.1
 * @apiName Register 
 * @apiGroup Auth
 *
 * @apiParam {String} email Valid email address.
 * @apiParam {String} password Min 8 characters, RegExp = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*?\W)[A-Za-z0-9\W]{8,}$/
 * @apiParam {String} first_name First name. Max 50 characters.
 * @apiParam {String} last_name Last name. Max 50 characters.
 * @apiParam {String} phone_number Phone number. Max 50 characters. RegExp = /^\+?[0-9]+([\s\-]?[0-9]+){7,}$/
 * @apiParam {String} postal_code Address postal code. Max 30 characters.
 * @apiParam {String} address_line_1 Address line 1. Max 50 characters.
 * @apiParam {String} [address_line_2] Address line 2. Max 50 characters.
 * @apiParam {String} city Address city. Max 50 characters.
 * @apiParam {String} [country] Address country. Max 50 characters.
 *
 * @apiSuccess (Success 201){Object} user User object.
 * @apiSuccess (Success 201){String} user._id User id.
 * @apiSuccess (Success 201){String} user.email User email.
 * @apiSuccess (Success 201){String} user.first_name User first name.
 * @apiSuccess (Success 201){String} user.last_name User last name.
 * @apiSuccess (Success 201){String[]="USER", "ADMIN"} user.roles User roles array.
 *
 * @apiSuccess (Success 201){Object} auth Auth object.
 * @apiSuccess (Success 201){String} auth.token Access token.
 * @apiSuccess (Success 201){String} auth.refresh_token Refresh token.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *          "user": {
 *              "_id": "5bfec11d18176846d01af945",
 *              "email": "test@test.com",
 *              "first_name": "Test",
 *              "last_name": "Test",
 *              "roles": [
 *                  "USER"
 *              ]
 *          },
 *          "auth": {
 *              "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NDQwMjcwMzcsImRhdGEiOnsiX2lkIjoiNWJmZWMxMWQxODE3Njg0NmQwMWFmOTQ1IiwiZW1haWwiOiJhZHJpYW4ubWFzbGVyekByZWFkeTRzLmNvbSJ9LCJpYXQiOjE1NDM0MjIyMzd9.NRcz7tzcCjSGiaMdLVspiAaHaR9Do23sRnMhkzuvDHw",
 *              "refresh_token": "VFNOh4mO5TcVV1JKHtLIurFGyFUsUODhNyJe8fQAKn4nkWHVpM1CGmTG0qt7jfeBdEem81UDmqbwXeO5VLibgH2LE2kvtFRiDazzwqLGazKuqU7Nqob6ofWNCFuId0dI"
 *          }
 *     }
 *
 * @apiUse WrongParametersError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "email",
 *                   "message": "The email has already been taken."
 *              }
 *          ]
 *      }
 */

/**
 * @api {post} /login Login
 * @apiSampleRequest /login
 * @apiVersion 0.0.1
 * @apiName Login
 * @apiGroup Auth
 *
 * @apiParam {String} [email] Email address. Required with password.
 * @apiParam {String} [password] User password. Required with email.
 * @apiParam {String} [refresh_token] Refresh token required to handle auth refresh. Required when email and password is not sent.
 * @apiParam {String="USER", "ADMIN"} [role] Role to login
 *
 * @apiSuccess {Object} user User object.
 * @apiSuccess {String} user._id User id.
 * @apiSuccess {String} user.email User email.
 * @apiSuccess {String} user.first_name User first name.
 * @apiSuccess {String} user.last_name User last name.
 * @apiSuccess {String[]="USER", "ADMIN"} user.roles User roles array.
 *
 * @apiSuccess {Object} auth Auth object.
 * @apiSuccess {String} auth.token Access token.
 * @apiSuccess {String} auth.refresh_token Refresh token.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "user": {
 *              "_id": "5bfec11d18176846d01af945",
 *              "email": "test@test.com",
 *              "first_name": "Test",
 *              "last_name": "Test",
 *              "roles": [
 *                  "USER"
 *              ]
 *          },
 *          "auth": {
 *              "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NDQwMjcwMzcsImRhdGEiOnsiX2lkIjoiNWJmZWMxMWQxODE3Njg0NmQwMWFmOTQ1IiwiZW1haWwiOiJhZHJpYW4ubWFzbGVyekByZWFkeTRzLmNvbSJ9LCJpYXQiOjE1NDM0MjIyMzd9.NRcz7tzcCjSGiaMdLVspiAaHaR9Do23sRnMhkzuvDHw",
 *              "refresh_token": "VFNOh4mO5TcVV1JKHtLIurFGyFUsUODhNyJe8fQAKn4nkWHVpM1CGmTG0qt7jfeBdEem81UDmqbwXeO5VLibgH2LE2kvtFRiDazzwqLGazKuqU7Nqob6ofWNCFuId0dI"
 *          }
 *     }
 *
 * @apiError (401 Authentication failed) {Object[]} errors Errors array.
 * @apiError (401 Authentication failed) {String} errors.field Error field name.
 * @apiError (401 Authentication failed) {String} errors.message Error message.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 401 Authentication failed
 *     {
 *          "errors": [
 *              {
 *                   "field": "auth",
 *                   "message": "Wrong email or password"
 *              }
 *          ]
 *      }
 */

/**
 * @api {post} /password/remind Forgot password
 * @apiSampleRequest /password/remind
 * @apiVersion 0.0.1
 * @apiName Forgot password
 * @apiGroup Auth
 *
 * @apiParam {String} email Email address.
 *
 * @apiUse SuccessOperationStatus
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 User not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "user",
 *                   "message": "User not found"
 *              }
 *          ]
 *      }
 */


/**
 * @api {put} /password/remind Forgot password change
 * @apiSampleRequest /password/remind
 * @apiVersion 0.0.1
 * @apiName Forgot password change
 * @apiGroup Auth
 *
 * @apiParam {String} password New password. Min 8 characters, RegExp = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*?\W)[A-Za-z0-9\W]{8,}$/
 * @apiParam {String} token Password remind token.
 *
 * @apiUse SuccessOperationStatus
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 User not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "user",
 *                   "message": "User not found"
 *              }
 *          ]
 *      }
 *
 * @apiUse WrongParametersError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "password",
 *                   "message": "Password field is required"
 *              }
 *          ]
 *      }
 *
 * @apiError (410 Token expired) {Object[]} errors Errors array.
 * @apiError (410 Token expired) {String} errors.field Error field name.
 * @apiError (410 Token expired) {String} errors.message Error message.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 410 Token expired
 *     {
 *          "errors": [
 *              {
 *                   "field": "token",
 *                   "message": "Token expired"
 *              }
 *          ]
 *      }
 */


