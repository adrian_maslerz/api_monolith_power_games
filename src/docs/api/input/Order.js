/**
 * @api {post} /orders Add order
 * @apiSampleRequest /orders
 * @apiVersion 0.0.1
 * @apiName Add order
 * @apiGroup Order
 *
 * @apiParam {String} token Stripe card token or card id from stripe.
 * @apiParam {Object[]} items Products to buy encoded as string, eg. [{"amount": 1, "product": "5f0af39a1c5d9d0049be2e22"}]
 * @apiParam {String} [email] Valid email address.
 * @apiParam {String} [first_name] First name. Max 50 characters.
 * @apiParam {String} [last_name] Last name. Max 50 characters.
 * @apiParam {String} [phone_number] Phone number. Max 50 characters. RegExp = /^\+?[0-9]+([\s\-]?[0-9]+){7,}$/
 * @apiParam {String} [postal_code] Address postal code. Max 30 characters.
 * @apiParam {String} [address_line_1] Address line 1. Max 50 characters.
 * @apiParam {String} [address_line_2] Address line 2. Max 50 characters.
 * @apiParam {String} [city] Address city. Max 50 characters.
 * @apiParam {String} [country] Country name. Max 50 characters.
 *
 * @apiSuccess (Success 201){Boolean} status Operation status.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "status": true,
 *         "_id": "5f0af39a1c5d9d0049be2e22"
 *     }
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "items",
 *                   "message": "No valid items"
 *              }
 *          ]
 *      }
 *
 * @apiUse WrongParametersError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "email",
 *                   "message": "Email is required"
 *              }
 *          ]
 *      }
 *
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 */

/**
 * @api {get} /orders Orders list
 * @apiSampleRequest /orders
 * @apiVersion 0.0.1
 * @apiName Orders list
 * @apiGroup Order
 *
 * @apiUse PaginationElements

 * @apiParam {String} [product] Filter by product id.
 * @apiParam {String="createdASC", "createdDESC", "totalASC", "totalDESC", "products_countASC", "products_countDESC", "pieces_countASC", "pieces_countDESC"} [sort="createdDESC"] Sort option.
 *
 * @apiSuccess {Object[]} results Orders list.
 *
 * @apiSuccess {String} results._id Order id
 * @apiSuccess {Number} results.created Order created date as timestamp ms.
 * @apiSuccess {Number} results.total Order total.
 * @apiSuccess {Number} results.products_count Order products count.
 * @apiSuccess {Number} results.pieces_count Order pieces count.
 *
 * @apiSuccess {Object} results.user User object
 * @apiSuccess {String} results.user._id User id
 * @apiSuccess {String} results.user.email User email
 *
 * @apiSuccess {Object} results.payment Payment object
 * @apiSuccess {Object} results.payment.card Card object
 * @apiSuccess {String} results.payment.card.card_id Card id from stripe
 * @apiSuccess {String} results.payment.card.brand Card brand
 * @apiSuccess {String} results.payment.card.last_4 Card last 4
 * @apiSuccess {String} results.payment.card.exp_month Card expiration month
 * @apiSuccess {String} results.payment.card.exp_year Card expiration year
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "results": [
 *              {
 *                  "_id": "5f133bef4d1c30006dc71c45",
 *                  "created": 1595096049248,
 *                  "total": 749.97,
 *                  "products_count": 2,
 *                  "pieces_count": 3,
 *                  "user": {
 *                      "_id": "5f133bef4d1c30006dc71c45",
 *                      "email": "test@test.com",
 *                  },
 *                  "payment": {
 *                      "card": {
 *                          "card_id": "card_1H5JbKIs9F98ahEkwhtVKqSq",
 *                          "brand": "Visa",
 *                          "last_4": "4242",
 *                          "exp_month": "7",
 *                          "exp_year": "2021"
 *                      }
 *                  }
 *              }
 *          ],
 *          "pages": 1,
 *          "total": 3,
 *     }
 * @apiUse AuthenticateElements
 **/

/**
 * @api {get} /orders/:id Order details
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Order details
 * @apiGroup Order
 *
 * @apiParam {String} id Order id.
 *
 * @apiSuccess {String} _id Order id
 * @apiSuccess {Number} created Order created date as timestamp ms.
 *
 * @apiSuccess {Object[]} items Items array
 * @apiSuccess {String} items._id Item id
 * @apiSuccess {String} items.codes Item codes array.
 * @apiSuccess {Boolean} items.sent Item codes availability.
 *
 * @apiSuccess {Object} items.product Product clone object
 * @apiSuccess {String} items.product._id Product clone
 * @apiSuccess {String} items.product.original_id Product clone original id
 * @apiSuccess {String} items.product.title Product clone title
 * @apiSuccess {String} items.product.image Product clone image
 * @apiSuccess {Number} items.product.price Product clone price
 * @apiSuccess {Number} items.product.premiere Product clone premiere as timestamp in ms.
 *
 * @apiSuccess {Object} [items.review] Review object
 * @apiSuccess {Number} items.product.rate Review rate
 * @apiSuccess {String} items.product.description Review description
 * @apiSuccess {Boolean} items.product.approved Review approved
 *
 * @apiSuccess {String} user_details.email User email.
 * @apiSuccess {String} user_details.first_name User first name.
 * @apiSuccess {String} user_details.last_name User last name.
 * @apiSuccess {String} user_details.phone_number User phone number.
 *
 * @apiSuccess {Object} user_details.address Address object
 * @apiSuccess {String} user_details.address.postal_code Address postal code
 * @apiSuccess {String} user_details.address.address_line_1 Address line 1
 * @apiSuccess {String} [user_details.address.address_line_2] Address line 2
 * @apiSuccess {String} user_details.address.city Address city
 * @apiSuccess {String} user_details.address.country Address country
 *
 * @apiSuccess {String} user User id reference.
 *
 * @apiSuccess {Object} payment Payment object
 * @apiSuccess {String="pln"} payment.currency Payment currency
 * @apiSuccess {String} payment.description Payment description
 * @apiSuccess {Number} payment.amount Payment amount
 * @apiSuccess {Number} payment.created Payment created date as timestamp ms.
 *
 * @apiSuccess {Object} payment.card Card object
 * @apiSuccess {String} payment.card.card_id Card id from stripe
 * @apiSuccess {String} payment.card.brand Card brand
 * @apiSuccess {String} payment.card.last_4 Card last 4
 * @apiSuccess {String} payment.card.exp_month Card expiration month
 * @apiSuccess {String} payment.card.exp_year Card expiration year
 *
 * @apiSuccess {Number} total Order total.
 * @apiSuccess {Number} products_count Order products count.
 * @apiSuccess {Number} pieces_count Order pieces count.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *          "_id": "5f133bef4d1c30006dc71c45",
 *          "created": 1595096049248,
 *          "items": [
 *              {
 *                  "sent": false,
 *                  "codes": [
 *                      "HSJBDBAJSBDJASBJFHB"
 *                  ],
 *                  "_id": "5f133bf14d1c30006dc71c49",
 *                  "product": {
 *                      "_id": "5f133bf04d1c30006dc71c47",
 *                      "original_id": "5f0af39a1c5d9d0049be2e22",
 *                      "title": "BATTLEFRONT II PS5",
 *                      "price": 249.99,
 *                      "premiere": 1573776000000,
 *                      "image": "localhost:8000/orders/uploads/products/5f133bf04d1c30006dc71c47/15950960488290.0028118717784799295.jpg"
 *                  },
 *                  "review": {
 *                      "rate": 3,
 *                      "description": "asda",
 *                      "approved": false
 *                  }
 *              }
 *          ],
 *          "user": "5f07999fa53df50111178b35",
 *          "payment": {
 *              "card": {
 *                  "card_id": "card_1H5JbKIs9F98ahEkwhtVKqSq",
 *                  "brand": "Visa",
 *                  "last_4": "4242",
 *                  "exp_month": "7",
 *                  "exp_year": "2021"
 *              },
 *              "currency": "pln",
 *              "description": "Payment for order 5f07999fa53df50111178b35",
 *              "amount": 10
 *          },
 *          "user_details": {
 *              "email": "adrianmaslerz@interia.eu",
 *              "first_name": "Adrian",
 *              "last_name": "Maślerz",
 *              "phone_number": "347985938",
 *              "address": {
 *                  "address_line_2": null,
 *                  "country": "PL",
 *                  "postal_code": "23423423",
 *                  "address_line_1": "asdasd",
 *                  "city": "wdsada"
 *              }
 *          },
 *          "total": 749.97,
 *          "products_count": 2,
 *          "pieces_count": 3
 *      }
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "order",
 *                   "message": "Order not found"
 *              }
 *          ]
 *      }
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 **/

/**
 * @api {post} /orders/:orderId/items/:itemId Add order item review
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Add order item review
 * @apiGroup Order
 *
 * @apiParam {String} orderId Order id.
 * @apiParam {String} itemId Item id.
 *
 * @apiParam {Number} rate Ordered item rate. Integer. Min 1. Max 5.
 * @apiParam {String} description Ordered item review description. Max 500 characters.
 *
 * @apiSuccess (Success 201){Boolean} status Operation status.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "status": true,
 *     }
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "item",
 *                   "message": "Item not found"
 *              }
 *          ]
 *      }
 * @apiUse WrongParametersError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "rate",
 *                   "message": "Rate is required"
 *              }
 *          ]
 *      }
 * @apiUse ConflictError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 409 Conflict
 *     {
 *          "errors": [
 *              {
 *                   "field": "review",
 *                   "message": "This item has already been reviewed"
 *              }
 *          ]
 *      }
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 **/

/**
 * @api {put} /orders/:orderId/items/:itemId Change review state
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Change review state
 * @apiGroup Order
 *
 * @apiParam {String} orderId Order id.
 * @apiParam {String} itemId Item id.
 *
 * @apiParam {Number=1, 0} approved Review approval
 *
 * @apiUse SuccessOperationStatus
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "item",
 *                   "message": "Item not found"
 *              }
 *          ]
 *      }
 * @apiUse ConflictError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 409 Conflict
 *     {
 *          "errors": [
 *              {
 *                   "field": "review",
 *                   "message": "This item is not reviewed"
 *              }
 *          ]
 *      }
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 **/

/**
 * @api {get} /orders/reviews Orders reviews
 * @apiSampleRequest /orders/reviews
 * @apiVersion 0.0.1
 * @apiName Orders reviews
 * @apiGroup Order
 *
 * @apiSuccess {Object[]} reviews Orders reviews list.
 *
 * @apiSuccess {Object} reviews.user User object
 * @apiSuccess {String} reviews.user.first_name User first name
 * @apiSuccess {String} reviews.user.last_name User last name
 *
 * @apiSuccess {Object} reviews.product Product object
 * @apiSuccess {String} reviews.product._id Product id
 * @apiSuccess {String} reviews.product.original_id Product original id
 * @apiSuccess {String} reviews.product.title Product title
 *
 * @apiSuccess {Object} reviews.review Review object
 * @apiSuccess {Number} reviews.review.rate Review rate
 * @apiSuccess {String} reviews.review.description Review description
 * @apiSuccess {Boolean} reviews.review.approved Review approved state
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "reviews": [
 *              {
 *                  "user": {
 *                      "first_name": "Test",
 *                      "last_name": "Test"
 *                  },
 *                  "product": {
 *                      "_id": "5f145a02f9839e001d99ef42",
 *                      "original_id": "5f0af39a1c5d9d0049be2e22",
 *                      "title": "BATTLEFRONT II PS5"
 *                  },
 *                  "review": {
 *                      "rate": 5,
 *                      "description": "dsaa",
 *                      "approved": true
 *                  }
 *              }
 *          ]
 *     }
 **/
