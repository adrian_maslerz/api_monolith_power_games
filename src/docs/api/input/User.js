/**
 * @api {get} /user/profile User profile
 * @apiSampleRequest /user/profile
 * @apiVersion 0.0.1
 * @apiName User profile
 * @apiGroup User
 *
 * @apiSuccess {String} _id User id.
 * @apiSuccess {String} email User email.
 * @apiSuccess {String} first_name User first name.
 * @apiSuccess {String} last_name User last name.
 * @apiSuccess {String} phone_number User phone number.
 * @apiSuccess {String[]="USER", "ADMIN"} roles User roles array.
 * @apiSuccess {String[]="USER_FULL", "ADMIN_FULL"} permissions User permissions array.
 *
 * @apiSuccess {Object} [address] Address object
 * @apiSuccess {String} address.postal_code Address postal code
 * @apiSuccess {String} address.address_line_1 Address line 1
 * @apiSuccess {String} [address.address_line_2] Address line 2
 * @apiSuccess {String} address.city Address city
 * @apiSuccess {String} address.country Address country
 *
 * @apiSuccess {Object} [customer] Address object
 * @apiSuccess {String} customer.stripe_id Customer stripe id
 * @apiSuccess {Object[]} customer.cards Cards objects
 * @apiSuccess {String} customer.cards._id Card id
 * @apiSuccess {String} customer.cards.card_id Card id from stripe
 * @apiSuccess {String} customer.cards.brand Card brand
 * @apiSuccess {String} customer.cards.last_4 Card last 4
 * @apiSuccess {String} customer.cards.exp_month Card expiration month
 * @apiSuccess {String} customer.cards.exp_year Card expiration year
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "_id": "5f07999fa53df50111178b35",
 *          "email": "test@test.com",
 *          "first_name": "Test",
 *          "last_name": "Test",
 *          "phone_number": "347985938",
 *          "roles": [
 *              "USER"
 *          ],
 *          "permissions": [],
 *          "address": {
 *              "postal_code": "23423423",
 *              "address_line_1": "asdasd",
 *              "address_line_2": null,
 *              "city": "wdsada",
 *              "country": "PL"
 *          },
 *          "customer": {
 *              "stripe_id": "cus_HeEgew5AczOecB",
 *              "cards": [
 *                  {
 *                      "_id": "5f0f706272627300f13eef4f",
 *                      "card_id": "card_1H5HqRIs9F98ahEkTBi9oVXf",
 *                      "brand": "Visa",
 *                      "last_4": "4242",
 *                      "exp_month": "7",
 *                      "exp_year": "2021"
 *                  }
 *              ]
 *          }
 *     }
 *
 * @apiUse AuthenticateElements
 */

/**
 * @api {put} /user/profile Update user profile
 * @apiSampleRequest /user/profile
 * @apiVersion 0.0.1
 * @apiName Update user profile
 * @apiGroup User
 *
 * @apiParam {String} [email] Valid email address.
 * @apiParam {String} [first_name] First name. Max 50 characters.
 * @apiParam {String} [last_name] Last name. Max 50 characters.
 * @apiParam {String} [phone_number] Phone number. Max 50 characters. RegExp = /^\+?[0-9]+([\s\-]?[0-9]+){7,}$/
 * @apiParam {String} [postal_code] Address postal code. Max 30 characters.
 * @apiParam {String} [address_line_1] Address line 1. Max 50 characters.
 * @apiParam {String} [address_line_2] Address line 2. Max 50 characters.
 * @apiParam {String} [city] Address city. Max 50 characters.
 * @apiParam {String} [country] Country name. Max 50 characters.
 *
 * @apiUse SuccessOperationStatus
 *
 * @apiUse WrongParametersError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong Parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "first_name",
 *                   "message": "First name is required"
 *              }
 *          ]
 *      }
 *
 * @apiUse AuthenticateElements
 */

/**
 * @api {put} /user/password Change password
 * @apiSampleRequest /user/password
 * @apiVersion 0.0.1
 * @apiName Change password
 * @apiGroup User
 *
 * @apiParam {String} old_password Old password.
 * @apiParam {String} new_password New password. Min 8 characters, RegExp = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*?\W)[A-Za-z0-9\W]{8,}$/
 *
 * @apiUse SuccessOperationStatus
 *
 * @apiUse WrongParametersError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong Parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "password",
 *                   "message": "Your old password is incorrect"
 *              }
 *          ]
 *      }
 *
 * @apiUse AuthenticateElements
 */

/**
 * @api {put} /user/email Change email
 * @apiSampleRequest /user/email
 * @apiVersion 0.0.1
 * @apiName Change email
 * @apiGroup User
 *
 * @apiParam {String} email New email.
 * @apiParam {String} password Current password.
 *
 * @apiUse SuccessOperationStatus
 *
 * @apiUse WrongParametersError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong Parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "email",
 *                   "message": "This email has already been taken"
 *              }
 *          ]
 *      }
 *
 * @apiUse AuthenticateElements
 */
