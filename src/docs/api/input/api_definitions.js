/**
 * @apiDefine AuthenticateElements
 *
 * @apiHeader {String} X-access-token Access token
 *
 * @apiError (401 Access Denied) {Object[]} errors Errors array.
 * @apiError (401 Access Denied) {String} errors.field Error field name.
 * @apiError (401 Access Denied) {String} errors.message Error message.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 401 Access Denied
 *     {
 *          "errors": [
 *              {
 *                   "field": "user",
 *                   "message": "Access Denied"
 *              }
 *          ]
 *      }
 *
 * @apiError (410 Token expired) {Object[]} errors Errors array.
 * @apiError (410 Token expired) {String} errors.field Error field name.
 * @apiError (410 Token expired) {String} errors.message Error message.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 410 Token expired
 *     {
 *          "errors": [
 *              {
 *                   "field": "token",
 *                   "message": "Access token expired"
 *              }
 *          ]
 *      }
 */

/**
 * @apiDefine AuthorizeElements
 *
 * @apiError (403 Permission denied) {Object[]} errors Errors array.
 * @apiError (403 Permission denied) {String} errors.field Error field name.
 * @apiError (403 Permission denied) {String} errors.message Error message.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 403 Permission denied
 *     {
 *          "errors": [
 *              {
 *                   "field": "permission",
 *                   "message": "Permission denied"
 *              }
 *          ]
 *      }
 */

/**
 * @apiDefine NotFoundError
 *
 * @apiError (404 Not found) {Object[]} errors Errors array.
 * @apiError (404 Not found) {String} errors.field Error field name.
 * @apiError (404 Not found) {String} errors.message Error message.
 *
 */

/**
 * @apiDefine WrongParametersError
 *
 * @apiError (406 Wrong parameters) {Object[]} errors Errors array.
 * @apiError (406 Wrong parameters) {String} errors.field Error field name.
 * @apiError (406 Wrong parameters) {String} errors.message Error message.
 *
 */

/**
 * @apiDefine ConflictError
 *
 * @apiError (409 Conflict) {Object[]} errors Errors array.
 * @apiError (409 Conflict) {String} errors.field Error field name.
 * @apiError (409 Conflict) {String} errors.message Error message.
 *
 */

/**
 * @apiDefine SuccessOperationStatus
 *
 * @apiSuccess (Success 200){Boolean} status Operation status.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": true
 *     }
 */

/**
 * @apiDefine PaginationElements
 * @apiParam {Number} [page=1] Page number.
 * @apiParam {Number} [results=10] Page results. Default 10.
 *
 * @apiSuccess {Object[]} results Pagination results
 * @apiSuccess {Number} pages Pagination total pages
 * @apiSuccess {Number} total Pagination total items
 */
