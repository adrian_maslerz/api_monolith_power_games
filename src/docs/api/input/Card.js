/**
 * @api {delete} /cards/:id Delete card
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Delete card
 * @apiGroup Card
 *
 * @apiParam {String} id Card id.
 *
 * @apiUse SuccessOperationStatus
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "card",
 *                   "message": "Card not found"
 *              }
 *          ]
 *      }
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 **/
