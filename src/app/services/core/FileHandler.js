// core
const multer = require('multer');
const fs = require('fs');
const axios = require("axios");
const mkdirp = require('mkdirp');

// custom
const Utils = require('../utilities');
const variables = require('../../../config/variables');

module.exports = class {

    constructor(req, res, config = { s3: false, s3AutoTransfer: false })
    {
        // main
        this.req = req;
        this.res = res;
        this.host = (req.secure || req.headers['x-forwarded-proto'] ? 'https://' : 'http://') + req.headers.host;

        // file
        this.filePath = null;

        // uploads
        this.uploadsFolder = variables.folders.uploads;
        this.uploadsPath = variables.paths.folders.uploads;

        // s3 handlers
        this.s3 = !config.s3 ? null : new aws.S3({
            accessKeyId: variables.services.aws.key,
            secretAccessKey: variables.services.aws.secret,
            region: variables.services.aws.region
        });
        this.s3AutoTransfer = config.s3AutoTransfer;
    }

    getFileUrl(filePath)
    {
        return /^http/.test(filePath) ? filePath : (/^http/.test(this.host) ? this.host : "http://" + this.host) + this.uploadsPath + '/' + filePath;
    }

    getFileFromUrl(url = '')
    {
        const serverUploadsPath = this.host + this.uploadsPath + '/';
        return url.replace(serverUploadsPath, '');
    }

    handleSingleUpload(field, path, settings = {})
    {
        const s3Fs = !this.s3 ? null : new S3FS(variables.services.aws.bucket, {
            accessKeyId: variables.services.aws.key,
            secretAccessKey: variables.services.aws.secret,
            region: variables.services.aws.region
        });

        let {upload, constrains} = getUploader(field, path, settings, s3Fs);
        upload = upload.single(field);

        // registers on response hook to clear upload from failed actions
        this.res.on('finish', () => {

            if (this.res.statusCode !== 200 && this.res.statusCode !== 201)
                this.clearUpload();

            // removing given file if this is replacement method
            if (this.req.file && constrains.fileToRemove && (this.res.statusCode === 200 || this.res.statusCode === 201))
                deleteFile(constrains.fileToRemove, {s3: Boolean(this.s3)});
        });

        // registers on response hook to move uploaded file to s3
        if (this.s3 && this.s3AutoTransfer) {
            this.res.on('finish', () => {
                if (this.res.statusCode === 200 || this.res.statusCode === 201) {
                    this.moveUploadedToS3(this.filePath);
                }
            });
        }

        return new Promise(resolve => {
            upload(this.req, this.res, (error) => {
                if (error)
                {
                    if (error.code === 'LIMIT_FILE_SIZE')
                        return this.res.status(406).json(Utils.parseStringError(`Uploaded file can't be bigger than ${constrains.maxFileSize}MB.`, error.field));

                    return this.res.status(406).json(Utils.parseStringError(error.message, error.field));
                }

                resolve(this.filePath = this.req.file ? path + '/' + this.req.file.filename : undefined);
            });
        });
    }

    handleMultiFilesUpload(field, path, settings = {})
    {
        let {upload, constrains} = getUploader(field, path, settings);
        upload = upload.array(field, constrains.maxFiles);

        // registers on response hook to clear upload from failed actions
        this.res.on('finish', () => {
            if (this.res.statusCode != 200 && this.res.statusCode != 201) {
                this.clearUpload();
            }
        });

        return new Promise((resolve) => {
            upload(this.req, this.res, (error) => {
                    if (error)
                    {
                        if (error.code == 'LIMIT_FILE_SIZE')
                            return this.res.status(406).json(Utils.parseStringError(`Uploaded file can\'t be bigger than ${constrains.maxFileSize}MB.`, error.field));

                        return this.res.status((error.status || 406)).json(Utils.parseStringError(error.message, error.field));
                    }

                    const paths = this.req.files ? this.req.files.map(file => path + '/' + file.filename) : undefined;
                    resolve(paths);
                }
            );
        });
    }

    static deleteFile(filePath, config = {s3: false})
    {
        deleteFile(filePath, config);
    }

    moveUploadedToS3(filePath, deleteLocalFile = true)
    {
        if (this.s3 && filePath) {
            fs.readFile(variables.folders.uploads + '/' + filePath, (error, data) => {
                if (error) {
                    console.log(error);
                } else {
                    const params = {
                        Body: data,
                        Key: filePath,
                        Bucket: variables.services.aws.bucket,
                        ContentType: mime.lookup(variables.folders.uploads + '/' + filePath) || undefined
                    };

                    this.s3.putObject(params, (error, result) => {
                        if (error) {
                            console.log(error);
                        }

                        if (!error && deleteLocalFile) {
                            deleteFile(filePath);
                        }

                        console.log('Moved');
                    });
                }
            });
        }
    }

    streamFileFromS3(filePath)
    {
        if (this.s3 && filePath) {
            const params = {
                Key: filePath,
                Bucket: variables.services.aws.bucket
            };

            this.s3
                .headObject(params, (error, data) => {
                    if (error) {
                        console.error('Fetch error', error.message);
                        if (error && !this.res.headersSent) {
                            return this.res.status(404).json(Utils.parseStringError('File not found', 'file'));
                        }
                    }

                    // range handle
                    if (this.req != null && this.req.headers.range != null) {
                        const range = this.req.headers.range;
                        const bytes = range.replace(/bytes=/, '').split('-');
                        const start = parseInt(bytes[0], 10);

                        const total = data.ContentLength;
                        const end = bytes[1] ? parseInt(bytes[1], 10) : total - 1;
                        const chunksize = (end - start) + 1;

                        this.res.set('Content-Range', 'bytes ' + start + '-' + end + '/' + total);
                        this.res.set('Accept-Ranges', 'bytes');
                        this.res.set('Content-Length', chunksize.toString());
                        this.res.set('Last-Modified', new Date(data.LastModified).toLocaleDateString());
                        this.res.set('Content-Type', data.ContentType);
                        this.res.set('ETag', data.ETag);

                        params.Range = range;
                    } else {
                        this.res.set('Content-Length', data.ContentLength);
                        this.res.set('Last-Modified', new Date(data.LastModified).toLocaleString());
                        this.res.set('Content-Type', data.ContentType);
                        this.res.set('ETag', data.ETag);
                    }

                    // streaming
                    const stream = this.s3.getObject(params).createReadStream();

                    stream.on('error', error => {
                        console.error('Streaming error', error.message);
                        if (error && !this.res.headersSent) {
                            return this.res.status(404).json(Utils.parseStringError('File not found', 'file'));
                        }
                    });

                    stream.on('end', () => {
                        console.log('Served by Amazon S3: ' + filePath);
                    });

                    stream.pipe(this.res);
                });
        }
    }

    async saveExternalSourceFile(url, path)
    {
        if(!url)
            return resolve(null);

        //ensure folder path
        const folderPath = variables.folders.uploads + "/" + path;
        mkdirp.sync(folderPath);

        const returnPath = path + "/" + new Date().getTime() + (Math.random() * Math.random()) + url.slice(url.lastIndexOf("."));
        const fullPath = variables.folders.uploads + "/" + returnPath;

        const response = await axios({
            method: 'GET',
            url: url,
            responseType: 'stream'
        });

        response.data.pipe(fs.createWriteStream(fullPath));

        return new Promise((resolve, reject) => {
            response.data.on('end', () => {
                resolve(returnPath)
            })

            response.data.on('error', () => {
                reject()
            })
        });
    }

    clearUpload()
    {
        // clears upload on unsuccessful
        if (this.req.file) {
            fs.unlink(this.req.file.path, (error) => (error) ? console.log(error) : removeEmptyDirectories(this.req.file.path));
        }

        // clears multi upload on unsuccessful
        if (this.req.files) {
            this.req.files.forEach(file => {
                fs.unlink(file.path, (error) => (error) ? console.log(error) : removeEmptyDirectories(file.path));
            });
        }
    }
};

function getUploader(field, path, settings = {}, s3Fs = null)
{
    const constrains = {
        maxFileSize: settings.maxFileSize || 5,
        allowedMimeTypes: settings.allowedMimeTypes || [],
        fileToRemove: settings.fileToRemove || false,
        skipCondition: settings.skipCondition || (() => false),
        maxFiles: settings.maxFiles || 5,
        appendTimestamp: settings.appendTimestamp || false,
        checkDuplicate: settings.checkDuplicate || false
    };

    const storage = multer.diskStorage({
        destination: variables.folders.uploads + '/' + path + '/',
        filename: (req, file, cb) => {
            let filename = '';

            if (constrains.appendTimestamp) {
                filename += new Date().getTime();
            }

            filename += file.originalname.replace(/\s/g, '-');
            filename = filename.replace(/[^\x00-\x7F]/g, ''); // clearing unsafe characters
            console.log('Filename', filename);

            if (constrains.checkDuplicate) {
                const fileSystem = s3Fs || fs;
                return fileSystem.exists(variables.folders.uploads + '/' + path + '/' + filename, (exist) => {
                    if (exist) {
                        return cb({status: 406, message: 'Duplicate file name.'});
                    } else {
                        return cb(null, filename);
                    }
                });
            } else {
                return cb(null, filename);
            }
        }
    });

    const upload = multer({
        storage: storage,
        limits: {
            fileSize: 1024 * 1024 * constrains.maxFileSize
        },
        fileFilter: async (req, file, cb) => {
            if (constrains.skipCondition()) {
                return cb(null, false);
            } else {
                const valid = constrains.allowedMimeTypes.indexOf(file.mimetype) != -1;
                return valid ? cb(null, true) : cb({
                    message: (field.charAt(0).toUpperCase() + field.slice(1).toLowerCase()).replace(/_/g, ' ') + ' extension is incorrect. Allowed extensions: ' + constrains.allowedMimeTypes.join(', ') + '.',
                    field: field
                });
            }
        }
    });

    return { upload, constrains };
}

function removeEmptyDirectories(path, s3Fs = null)
{
    const currentFolderPath = path.substr(0, path.replace(/\\/g, '/').lastIndexOf('/'));
    const fsSystem = s3Fs || fs;

    fsSystem.readdir(currentFolderPath, (error, files) => {
        if (!error && files && !files.length && currentFolderPath) {
            console.log('Removing', currentFolderPath);
            fsSystem.rmdir(currentFolderPath, (error) => {
                if (!error) {
                    removeEmptyDirectories(currentFolderPath, s3Fs);
                }
            });
        }
    });
}

function deleteFile(filePath, config = {s3: false})
{
    if (filePath) {
        if (config.s3)
        {
            // creating s3 handler
            const s3Fs = !config.s3 ? null : new S3FS(variables.services.aws.bucket, {
                accessKeyId: variables.services.aws.key,
                secretAccessKey: variables.services.aws.secret,
                region: variables.services.aws.region
            });

            s3Fs.unlink(filePath, (error) => (error) ? console.log(error) : removeEmptyDirectories(filePath, s3Fs));
        } else
            fs.unlink(variables.folders.uploads + '/' + filePath, (error) => (error) ? console.log(error) : removeEmptyDirectories(variables.folders.uploads + '/' + filePath));
    }
}
