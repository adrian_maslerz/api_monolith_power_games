//services
const mailer = require("./../../../config/mailer").getMailer();
const variables = require("./../../../config/variables");

module.exports = {

    passwordResetMail: (data) => {

        mailer.send(
            data.email,
            'Password reset',
            "forgot-password",
            data.name,
            {
                link: (data.admin ? variables.app.admin_domain : variables.app.app_domain) + variables.paths.emails.PASSWORD_RESET.replace(":token", data.token),
                ...data
            }
        );
    },

    accountCreated: (data) => {

        mailer.send(
            data.email,
            'Account created',
            "account-created",
            data.name,
            data
        );
    },

    contactMessage: (data) => {

        mailer.send(
            variables.app.contact_mail,
            'Contact message',
            "contact-message",
            "",
            data
        );
    },

    orderPlaced: (data) => {

        mailer.send(
            data.email,
            'Order placed',
            "order-placed",
            data.name,
            data
        );
    },

    gameCodes: (data) => {

        mailer.send(
            data.email,
            'Game codes',
            "game-codes",
            data.name,
            data
        );
    },
}
