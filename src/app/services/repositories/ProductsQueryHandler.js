
//services
const DateHandler = require("./../feature/DateHandler");
const RageHandler = require("./../feature/RageHandler");
const CollectionHandler = require("./../feature/CollectionHandler");
const CategoriesQueryHandler = require("./../repositories/CategoriesQueryHandler");

//models
const {
    ageCategories: AvailableAgeCategories,
    languages: AvailableLanguages,
    releaseTypes: AvailableReleaseTypes,
    gameModes: AvailableGameModes
} = require("./../../models/Product");

module.exports = {

    productsListHandle: (req, category = null) => {

        // params
        const search = req.query.search || '';
        const pattern = new RegExp('^.*' + search + '.*$');

        //filters by collections
        const languages = CollectionHandler.prepareCollection(req.query["languages"], AvailableLanguages);
        const ageCategories = CollectionHandler.prepareCollection(req.query["age_categories"], AvailableAgeCategories);
        const gameModes = CollectionHandler.prepareCollection(req.query["game_modes"], AvailableGameModes);
        const releaseTypes = CollectionHandler.prepareCollection(req.query["release_types"], AvailableReleaseTypes);

        //categories
        const { lookups } = CategoriesQueryHandler.prepareCategoriesToParentLookup(2);

        const priceRange = RageHandler.prepareNumberRageForField(req.query, "price");
        const premiereRange = DateHandler.prepareDateRageForField(req.query, "premiere");

        const pipeline = [
            {
                $match: {
                    title: { $regex: pattern, $options: "xi" },
                    ...priceRange.length ? { $and: priceRange } : {},
                    ...premiereRange.length ? { $and: premiereRange } : {},
                    ...languages.length ? { languages: { $in: languages} } : {},
                    ...ageCategories.length ? { age_category: { $in: ageCategories} } : {},
                    ...gameModes.length ? { game_modes: { $in: gameModes} } : {},
                    ...releaseTypes.length ? { release_type: { $in: releaseTypes} } : {},
                }
            },
            {
                $lookup: {
                    from: "categories",
                    as: "category",
                    let: { foreignId: "$category" },
                    pipeline: [
                        {
                            $match: { $expr: { $eq: ["$_id", "$$foreignId"] } }
                        },
                        lookups,
                        {
                            $project: {
                                title: 1,
                                parent: 1,
                                categories_tree: {
                                    $concatArrays: [
                                        {
                                            $cond: {
                                                if: { $size: "$parent"},
                                                then: {
                                                    $arrayElemAt: ["$parent.categories_tree", 0]
                                                },
                                                else: []
                                            }
                                        },
                                        ["$_id"]
                                    ]
                                },
                                created: 1
                            }
                        }
                    ]
                }
            },
            { $unwind: "$category" },
            {
                $match: {
                    _id: { $exists: true },
                    ...category ? { "category.categories_tree": category._id } : {}
                }
            },
            {
                $project: {
                    title: 1,
                    image: 1,
                    "category._id": 1,
                    "category.created": 1,
                    "category.title": 1,
                    intro: 1,
                    manufacturer: 1,
                    price: 1,
                    premiere: 1,
                    availability: 1,
                    rate: 1,
                    created: 1
                }
            }
        ];

        return { pipeline };
    }
};
