
module.exports = {

    ordersListHandle: () => {

        const pipeline = [
            {
                $lookup: {
                    from: "users",
                    as: "user",
                    let: { foreignId: "$user" },
                    pipeline: [
                        {
                            $match: {$expr: {$eq: ["$_id", "$$foreignId"]}}
                        },
                        {
                            $project: {
                                _id: 1,
                                email: 1,
                            }
                        }
                    ]
                }
            },
            {
                $unwind: "$user"
            },
            {
                $project: {
                    total: 1,
                    "payment.card": 1,
                    user: 1,
                    products_count: { $size: "$items"},
                    pieces_count: {
                        $size: {
                            $reduce: {
                                input: "$items.codes",
                                initialValue: [],
                                in: { $concatArrays: ["$$value", "$$this"]}
                            }
                        }
                    },
                    created: 1
                }
            }
        ];

        return { pipeline };
    },

    calculateProductRate: (id) => {
        return [
            {
                $unwind: "$items"
            },
            {
                $match: {
                    "items.review.approved": true
                }
            },
            {
                $project: {
                    product: "$items.product",
                    rate: "$items.review.rate"
                }
            },
            {
                $lookup: {
                    from: "productcopies",
                    as: "product",
                    let: { foreignId: "$product" },
                    pipeline: [
                        {
                            $match: {$expr: {$eq: ["$_id", "$$foreignId"]}}
                        },
                        {
                            $project: {
                                _id: 1,
                                original_id: 1,
                            }
                        }
                    ]
                }
            },
            {
                $unwind: "$product"
            },
            {
                $match: {
                    "product.original_id": id
                }
            },
            {
                $group: {
                    _id: "$product.original_id",
                    rate: { $avg: "$rate"}
                }
            }
        ]
    },

    reviewsList: () => {
        return [
            {
                $match: {
                    "items.review.approved": true
                }
            },
            {
                $unwind: "$items"
            },
            {
                $match: {
                    "items.review.approved": true
                }
            },
            {
                $lookup: {
                    from: "productcopies",
                    as: "product",
                    let: { foreignId: "$items.product" },
                    pipeline: [
                        {
                            $match: {$expr: {$eq: ["$_id", "$$foreignId"]}}
                        },
                        {
                            $project: {
                                _id: 1,
                                original_id: 1,
                                title: 1
                            }
                        }
                    ]
                }
            },
            {
                $unwind: "$product"
            },
            {
                $sort: { created: -1, _id: 1 }
            },
            {
                $limit: 3
            },
            {
                $project: {
                    _id: 0,
                    review: "$items.review",
                    user: {
                        first_name: "$user_details.first_name",
                        last_name: "$user_details.last_name",
                    },
                    product: 1
                }
            }
        ];
    }
};
