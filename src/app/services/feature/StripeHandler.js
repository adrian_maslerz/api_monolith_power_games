
//modules
const async = require("async");
const stripe = require("stripe");

//config
const variables = require("./../../../config/variables");

module.exports = class
{
    constructor()
    {
        this.stripe = stripe(variables.services.stripe.key);
    }

    //customers
    createCustomer(user)
    {
        const params = {
            email: user.email,
            metadata: {
                user_id: user._id.toString()
            }
        };

        return this.stripe.customers.create(params);
    }

    //cards
    createCard(customer, token)
    {
        const params = {
            source: token
        };

        return this.stripe.customers.createSource(customer.stripe_id, params);
    }

    deleteCard(customer, card)
    {
        return this.stripe.customers.deleteSource(customer.stripe_id, card.card_id);
    }

	//charges
    createCharge(card, amount, customer, order)
    {
        return this.stripe.charges.create({
            amount: Math.round(amount * 100),
            currency: "pln",
            customer: customer.stripe_id,
            description: "Payment for order " + order,
            source: card.card_id
        });
    }

}
