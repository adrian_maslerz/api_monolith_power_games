//core
const bcrypt = require('bcrypt-nodejs');
const randomstring = require("randomstring");
const JWT = require('jsonwebtoken');
const async = require('async');

// config
const { roles } = require('../../../config/access-control');

//services
const MailHandler = require('./../core/MailHandler');

const getLoginUserParser = (user, auth) => {
    return {
        user: {
            _id: user._id,
            email: user.email,
            first_name: user.first_name,
            last_name: user.last_name,
            roles: user.roles
        },
        auth: {
            token: auth.token,
            refresh_token: auth.refresh_token
        }
    }
}

module.exports = {

    handleLogin: (req, model) => {
        return new Promise(async (resolve, reject) => {
            const query = {};

            // role handle
            const role = roles.map(role => role.role).includes(req.body.role || req.query.role) ? req.body.role || req.query.role : null;
            if (role)
                query.roles = role;

            // preparing query
            if (req.body.email && req.body.password)
                query.email = req.body.email;
            else if (req.body.refresh_token)
                query['auth.refresh_token'] = req.body.refresh_token;
            else
                return reject({ code: 401, message: 'Wrong email or password', field: 'auth' });

            // getting user
            const user = await model.findOne(query).exec();
            if (!user)
                return reject({ code: 401, message: 'Wrong email or password', field: 'auth' });

            // refresh token login handle
            if (query['auth.refresh_token'])
            {
                const auth = await user.generateAccessTokens();
                user.auth = user.auth.filter(auth => auth.refresh_token != req.body.refresh_token); // removing current refresh token
                user.save().catch(error => console.log(error));

                return resolve(await getLoginUserParser(user, auth));
            }

            // password login
            else
            {
                bcrypt.compare(req.body.password, user.password, async (error, status) => {

                    if (!status)
                        return reject({ code: 401, message: 'Wrong email or password', field: 'auth' });

                    const auth = await user.generateAccessTokens();
                    user.save().catch(error => console.log(error));
                    return resolve(await getLoginUserParser(user, auth));
                });
            }
        });
    },

    getLoginUser: getLoginUserParser,

    handlePasswordRemind: (req, model) => {
        return new Promise(async (resolve, reject) => {

            // getting user
            const user = await model.findOne({ email: req.body.email }).exec();
            if (!user)
                return reject({ code: 404, message: 'User not found', field: 'user' });

            // resolving promise
            resolve();

            // generating new reminder and sending email
            const data = user.addPasswordRemindHandle();
            MailHandler.passwordResetMail(data);

            user.save().catch(error => console.log(error));
        });
    },

    handlePasswordRemindChange: (req, model) => {

        return new Promise(async (resolve, reject) => {

            // getting user by password reset token
            const token = req.body.token || req.query.token;
            const user = await model.findOne({ 'password_resets.token': token }).exec();
            if (!user || !token)
                return reject({ code: 404, message: 'User not found', field: 'user' });

            const passwordReset = user.password_resets.find(password_reset => password_reset.token === token);

            // checking expiration
            if (passwordReset.expiration < new Date())
                return reject({ code: 410, message: 'Link expired', field: 'link' });

            user.password = req.body.password;
            user.validate()
                .then(() => {

                    // sending resolve
                    resolve();

                    // hashing password and removing password resets
                    bcrypt.hash(user.password, null, null, (error, hash) => {

                        if (error)
                            return reject({ code: 500, message: 'Cannot hash password', field: 'password' });

                        user.set({ password: hash, password_resets: [] });
                        user.save().catch(error => console.log(error));
                    });
                })
                .catch(error => reject({ code: 406, error }));
        });
    },

    generateAccessTokens: (user, accessTokenExpirationDays) => {

        return new Promise(resolve => {

            // preparing permissions
            let permissions = user.permissions;
            user.roles.forEach(role => {
                const configRole = roles.find(configRole => configRole.role == role);
                const rolePermissions = configRole ? configRole.permissions : [];
                permissions = [...permissions, ...rolePermissions];
            });

            // refreshes access tokens
            const key = randomstring.generate(128);
            const auth = {
                signature_key: key,
                refresh_token: randomstring.generate(128),
                token: JWT.sign({
                    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * accessTokenExpirationDays),
                    data: {
                        _id: user._id,
                        email: user.email,
                        permissions: permissions
                    }
                }, key)
            };

            // removing expired tokens
            async.filter(user.auth, (auth, callback) => {
                JWT.verify(auth.token, auth.signature_key, (error) => {
                    if (error && error.name === 'TokenExpiredError')
                        return callback(null, false);

                    callback(null, true);
                });
            }, (errors, results) => resolve({ auth, results }));
        });
    }
}
