
const prepareValidInputDateFunction = (input) => {
    return  !isNaN(Date.parse(input))? new Date(input) : !isNaN(parseInt(input))? new Date(parseInt(input)) : false;
}

module.exports = {
    prepareValidInputDate: prepareValidInputDateFunction,

    prepareDateRageForField: (params, fieldName, pipelineQuery = true) => {

        let start = prepareValidInputDateFunction(params[fieldName + "_from"]);
        let end = prepareValidInputDateFunction(params[fieldName + "_to"]);

        //ensure start is greater than end
        if(start && end && start.getTime() > end.getTime())
        {
            const temp = end;
            end = start;
            start = temp;
        }

        if(!pipelineQuery)
            return { start, end }

        return [
            ...start ? [{ [fieldName]: { $gte: start } }] : [],
            ...end ? [{ [fieldName]: { $lte: end } }] : [],
        ]
    }
}

