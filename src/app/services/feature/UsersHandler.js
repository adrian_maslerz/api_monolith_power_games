//core
const bcrypt = require('bcrypt-nodejs');

module.exports = {
    handleChangePassword: (req) => {

        return new Promise((resolve, reject) => {

            bcrypt.compare(req.body["old_password"], req.user.password, (error, status) => {

                //wrong password
                if (!status)
                    return reject({ code: 406, message: "Your old password is incorrect", field: "password"});

                req.user.password = req.body["new_password"];
                req.user
                    .validate()
                    .then(() => {

                        //sending resolve
                        resolve();

                        //hashing password and saving user
                        bcrypt.hash(req.user.password, null, null, (error, hash) => {
                            req.user.password = hash;
                            req.user.save().catch(error => console.log(error));
                        });
                    })
                    .catch(error => reject({ code: 406, error: error }));
            });
        });
    }
}