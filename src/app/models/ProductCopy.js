// core
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

// schema
const schema = mongoose.Schema({
    original_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, '{PATH} is required.'],
    },
    title: {
        type: String,
        required: [true, '{PATH} is required.'],
    },
    category: {
        type: String,
        required: [true, '{PATH} is required.']
    },
    image: {
        type: String,
        required: [true, '{PATH} is required.'],
    },
    manufacturer: {
        type: String,
        required: [true, '{PATH} is required.'],
    },
    intro: {
        type: String,
        required: [true, '{PATH} is required.'],
    },
    description: {
        type: String,
        required: [true, '{PATH} is required.'],
    },
    price: {
        type: Number,
        required: [true, '{PATH} is required.']
    },
    age_category: {
        type: String,
        required: [true, '{PATH} is required.'],
    },
    languages: [
        {
            type: String,
            required: [true, '{PATH} is required.'],
        }
    ],
    game_modes: [
        {
            type: String,
            required: [true, '{PATH} is required.']
        }
    ],
    release_type: {
        type: String,
        required: [true, '{PATH} is required.']
    },
    premiere: {
        type: Date,
        required: [true, '{PATH} is required.'],
    },
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date,
        default: Date.now()
    }
}, { usePushEach: true });

// middlewares
schema.pre('save', function (next) {

    // dates handle
    const product = this;
    if (product.isNew)
        product.created = new Date();
    if (product.isModified())
        product.updated = new Date();

    next();
});

schema.post('remove', function (product)
{
    const FileHandler = require("./../services/core/FileHandler");
    FileHandler.deleteFile(product.image);
});

// statics
schema.statics.parse = function (product, req)
{
    //dates
    if (product.created)
        product.created = product.created.getTime();

    if (product.premiere)
        product.premiere = product.premiere.getTime();

    //image
    if(product.image)
    {
        const FileHandler = require("./../services/core/FileHandler");
        const handler = new FileHandler(req);
        product.image = handler.getFileUrl(product.image);
    }

    return product;
};

schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

module.exports = { ProductCopy: mongoose.model('ProductCopy', schema) };
