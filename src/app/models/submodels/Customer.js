// core
const mongoose = require('mongoose');

// schema
const schema = mongoose.Schema({
    stripe_id: {
        type: String,
        required: [true, '{PATH} is required.']
    },
    cards: [
        {
            card_id: {
                type: String,
                required: [true, '{PATH} is required.'],
            },
            brand: {
                type: String,
                required: [true, '{PATH} is required.'],
            },
            last_4: {
                type: String,
                required: [true, '{PATH} is required.'],
            },
            exp_month: {
                type: String,
                required: [true, '{PATH} is required.'],
            },
            exp_year: {
                type: String,
                required: [true, '{PATH} is required.'],
            }
        }
    ],
});

module.exports = { Customer: schema };
