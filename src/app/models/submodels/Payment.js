// core
const mongoose = require('mongoose');

// schema
const schema = mongoose.Schema({
    card: {
        card_id: {
            type: String,
            required: [true, '{PATH} is required.'],
        },
        brand: {
            type: String,
            required: [true, '{PATH} is required.'],
        },
        last_4: {
            type: String,
            required: [true, '{PATH} is required.'],
        },
        exp_month: {
            type: String,
            required: [true, '{PATH} is required.'],
        },
        exp_year: {
            type: String,
            required: [true, '{PATH} is required.'],
        }
    },
    currency: {
        type: String,
        required: [true, '{PATH} is required.'],
        enum: ["pln"]
    },
    description: {
        type: String,
        required: [true, '{PATH} is required.']
    },
    amount: {
        type: Number,
        required: [true, '{PATH} is required.'],
        min: [0, '{PATH} cannot be lower than {MIN}.']
    }
});

module.exports = { Payment: schema };
