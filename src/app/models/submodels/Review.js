// modules
const mongoose = require('mongoose');

// schema
const schema = mongoose.Schema({
    rate: {
        type: Number,
        required: [true, '{PATH} is required.'],
        min: [1, '{PATH} cannot be lower than {MIN}.'],
        max: [5, '{PATH} cannot be higher than {MAX}.'],
    },
    description: {
        type: String,
        required: [true, '{PATH} is required.'],
        trim: true,
        maxlength: [500, "{PATH} can't be longer than {MAXLENGTH} characters."]
    },
    approved: {
        type: Boolean,
        default: false
    }
});

module.exports = { Review: schema };
