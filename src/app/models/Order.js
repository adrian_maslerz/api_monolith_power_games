// core
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

//services
const validators = require("./../services/validators");

// sub models
const { Address } = require('./submodels/Address');
const { Review } = require('./submodels/Review');
const { Payment } = require('./submodels/Payment');

// schema
const schema = mongoose.Schema({
    items: [
        {
            product: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'ProductCopy',
                required: [true, '{PATH} is required.']
            },
            sent: {
                type: Boolean,
                default: false
            },
            review: Review,
            codes: [
                {
                    type: String,
                    required: [true, '{PATH} is required.']
                }
            ]
        }
    ],
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: [true, '{PATH} is required.']
    },
    payment: {
        type: Payment,
        required: [true, '{PATH} is required.']
    },
    user_details: {
        email: {
            type: String,
            required: [true, '{PATH} is required.'],
            validate: validators.email,
            trim: true,
            lowercase: true,
        },
        first_name: {
            type: String,
            required: [true, '{PATH} is required.'],
            trim: true,
            maxlength: [50, "{PATH} can't be longer than {MAXLENGTH} characters."]
        },
        last_name: {
            type: String,
            required: [true, '{PATH} is required.'],
            trim: true,
            maxlength: [50, "{PATH} can't be longer than {MAXLENGTH} characters."]
        },
        phone_number: {
            type: String,
            required: [true, '{PATH} is required.'],
            trim: true,
            validate: validators.phoneNumber,
            maxlength: [50, "{PATH} can't be longer than {MAXLENGTH} characters."]
        },
        address: {
            type: Address,
            required: [true, '{PATH} is required.']
        }
    },
    total: {
        type: Number,
        required: [true, '{PATH} is required.']
    },
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date,
        default: Date.now()
    }
}, { usePushEach: true });

// middlewares
schema.pre('save', function (next) {

    // dates handle
    const order = this;
    if (order.isNew)
        order.created = new Date();
    if (order.isModified())
        order.updated = new Date();

    next();
});

// statics
schema.statics.parse = function (order, req)
{
    //dates
    if (order.created)
        order.created = order.created.getTime();

    if(order.items)
    {
        const { Product } = require("./Product");
        order.items.map(item => {
            item.product = Product.parse(item.product, req);
            return item;
        })
        order.products_count = order.items.length;
        order.pieces_count = order.items.reduce((allCodes, item) => [...allCodes, ...item.codes ], []).length;
    }

    return order;
};

schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

module.exports = { Order: mongoose.model('Order', schema) };
