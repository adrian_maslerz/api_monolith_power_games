// core
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const validators = require("./../services/validators")

const ageCategories = {
    SEVEN: "7",
    TWELVE: "12",
    SIXTEEN: "16",
    EIGHTEEN: "18",
    NO_INFO: "No information"
};

const languages = {
    POLISH: "Polish",
    ENGLISH: "English",
    GERMAN: "German",
    FRENCH: "French",
    SPANISH: "Spanish",
    ITALIAN: "Italian",
    JAPANESE: "Japanese"
};

const gameModes = {
    SINGLE_PLAYER: "Single player",
    MULTIPLAYER: "Multiplayer",
    MASSIVE_ONLINE_MULTIPLAYER: "Massive online multiplayer",
};

const releaseTypes = {
    BASIC: "Basic",
    DLC_ADDITION: "DLC addition",
    GOTY_EDITION: "GOTY edition",
    VR: "VR"
};

// schema
const schema = mongoose.Schema({
    title: {
        type: String,
        required: [true, '{PATH} is required.'],
        trim: true,
        uppercase: true,
        maxlength: [100, "{PATH} can't be longer than {MAXLENGTH} characters."]
    },
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category',
        required: [true, '{PATH} is required.']
    },
    image: {
        type: String,
        required: [true, '{PATH} is required.'],
    },
    manufacturer: {
        type: String,
        required: [true, '{PATH} is required.'],
        trim: true,
        uppercase: true,
        maxlength: [100, "{PATH} can't be longer than {MAXLENGTH} characters."]
    },
    availability: {
        type: Number,
        validators: validators.integer,
        required: [true, '{PATH} is required.'],
        min: [0, '{PATH} cannot be lower than {MIN}.']
    },
    intro: {
        type: String,
        required: [true, '{PATH} is required.'],
        trim: true
    },
    description: {
        type: String,
        required: [true, '{PATH} is required.'],
        trim: true
    },
    price: {
        type: Number,
        required: [true, '{PATH} is required.'],
        min: [0, '{PATH} cannot be lower than {MIN}.']
    },
    rate: {
        type: Number,
        default: 0
    },
    age_category: {
        type: String,
        required: [true, '{PATH} is required.'],
        enum: Object.values(ageCategories)
    },
    languages: {
        type: [
            {
                type: String,
                required: [true, '{PATH} is required.'],
                enum: Object.values(languages)
            },
        ],
        required: [true, "At least one {PATH} option is required."],
    },
    game_modes: {
        type: [
            {
                type: String,
                required: [true, '{PATH} is required.'],
                enum: Object.values(gameModes)
            },
        ],
        required: [true, "At least one {PATH} option is required."],
    },
    release_type: {
        type: String,
        required: [true, '{PATH} is required.'],
        enum: Object.values(releaseTypes)
    },
    premiere: {
        type: Date,
        required: [true, '{PATH} is required.'],
    },
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date,
        default: Date.now()
    }
}, { usePushEach: true });

// middlewares
schema.pre('save', function (next) {

    // dates handle
    const product = this;
    if (product.isNew)
        product.created = new Date();
    if (product.isModified())
        product.updated = new Date();

    next();
});

schema.post('remove', function (product)
{
    const FileHandler = require("./../services/core/FileHandler");
    FileHandler.deleteFile(product.image);
});

// statics
schema.statics.parse = function (product, req)
{
    //dates
    if (product.created)
        product.created = product.created.getTime();

    if (product.premiere)
        product.premiere = product.premiere.getTime();

    //image
    if(product.image)
    {
        const FileHandler = require("./../services/core/FileHandler");
        const handler = new FileHandler(req);
        product.image = handler.getFileUrl(product.image);
    }

    //category
    if(product.category)
    {
        const { Category } = require("./Category");
        product.category = Category.parse(product.category, req);
    }

    return product;
};

schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

module.exports = { Product: mongoose.model('Product', schema), languages, ageCategories, gameModes, releaseTypes };
