// core
const randomstring = require('randomstring');
const bcrypt = require('bcrypt-nodejs');
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

// configs
const { permissions, roles } = require('../../config/access-control');

// services
const AuthHandler = require('../services/feature/AuthHandler');
const validators = require('../services/validators');

// sub models
const { Address } = require('./submodels/Address');
const { Customer } = require('./submodels/Customer');

// settings
const accessTokenExpirationDays = 7;
const passwordResetExpirationDays = 7;
const passwordRegExp = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*?\W)[A-Za-z0-9\W]{8,}$/;

// schema
const schema = mongoose.Schema({
    email: {
        type: String,
        required: [true, '{PATH} is required.'],
        validate: validators.email,
        trim: true,
        lowercase: true,
        unique: true
    },
    password: {
        type: String,
        required: [true, '{PATH} is required.'],
        minlength: [8, '{PATH} should have at least {MINLENGTH} characters.'],
        validate: validators.password(passwordRegExp, '{PATH} must have at least 8 characters and contain at least one letter, number and special character.')
    },
    first_name: {
        type: String,
        required: [true, '{PATH} is required.'],
        trim: true,
        maxlength: [50, "{PATH} can't be longer than {MAXLENGTH} characters."]
    },
    last_name: {
        type: String,
        required: [true, '{PATH} is required.'],
        trim: true,
        maxlength: [50, "{PATH} can't be longer than {MAXLENGTH} characters."]
    },
    phone_number: {
        type: String,
        required: validators.required_if_not('roles.0', "ADMIN"),
        trim: true,
        validate: validators.phoneNumber,
        maxlength: [50, "{PATH} can't be longer than {MAXLENGTH} characters."]
    },
    address: {
        type: Address,
        required: validators.required_if_not('roles.0', "ADMIN"),
    },
    customer: Customer,
    auth: [
        {
            token: {
                type: String,
                default: null
            },
            refresh_token: {
                type: String,
                default: null
            },
            signature_key: {
                type: String,
                default: null
            }
        }
    ],
    password_resets: [
        {
            token: {
                type: String,
                default: randomstring.generate(128)
            },
            expiration: {
                type: Date,
                default: +new Date() + (passwordResetExpirationDays * 24 * 60 * 60 * 1000)
            }
        }
    ],
    roles: [
        {
            type: String,
            required: true,
            enum: roles.map(role => role.role)
        }
    ],
    permissions: [
        {
            type: String,
            required: true,
            enum: permissions
        }
    ],
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date,
        default: Date.now()
    }
}, { usePushEach: true });

// middlewares
schema.pre('save', function (next) {

    // dates handle
    const user = this;
    if (user.isNew)
        user.created = new Date();
    if (user.isModified())
        user.updated = new Date();

    next();
});

// methods
schema.methods.generateAccessTokens = function ()
{
    const user = this;
    return AuthHandler
        .generateAccessTokens(user, accessTokenExpirationDays)
        .then(resolve => {
            user.auth = resolve.results || [];
            user.auth.push(resolve.auth);

            return resolve.auth;
        });
};

schema.methods.hashPassword = function ()
{
    const user = this;
    return new Promise((resolve, reject) => {
        bcrypt.hash(user.password, null, null, (error, hash) => {
            if (error) {
                console.error(error);
                reject(error);
            }

            user.password = hash;
            resolve();
        });
    });
};

schema.methods.addPasswordRemindHandle = function ()
{
    const user = this;
    const token = randomstring.generate(128);
    user.password_resets.push({
        _id: mongoose.Types.ObjectId(),
        token: token,
        expiration: +new Date() + (passwordResetExpirationDays * 24 * 60 * 60 * 1000)
    });

    const response = {
        token: token,
        email: user.email,
        name: user.first_name,
        admin: user.roles.includes("ADMIN")
    };

    return response;
};

// statics
schema.statics.parse = function (user, req)
{
    // dates
    if (user.created)
        user.created = user.created.getTime();

    return user;
};

schema.plugin(uniqueValidator, {message: 'The {PATH} has already been taken.'});
schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

module.exports = { User: mongoose.model('User', schema) };
