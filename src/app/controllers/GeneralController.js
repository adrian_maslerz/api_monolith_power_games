
//services
const MailHandler = require('../services/core/MailHandler');

module.exports = class GeneralController
{
	async contact(req, res)
	{
		let errors;

		//validation
		req.check("message").notEmpty().withMessage('Message is required.').isLength({ max: 500 }).withMessage('Message cannot be longer than 500 characters.');
		req.check("email").notEmpty().withMessage('Email is required.').isEmail().withMessage('This is not valid email address');
		req.check("subject").notEmpty().withMessage('Subject is required.').isLength({ max: 200 }).withMessage('Subject cannot be longer than 200 characters.');

		if (errors = req.validationErrors())
			return res.status(406).json({ errors: errors });

		//sending email
		const data = {
			subject: req.body.subject,
			email: req.body.email,
			message: req.body.message
		}
		MailHandler.contactMessage(data);

		//sending response
		res.status(201).json({ status: true });

	}
}
