
//services
const Utils = require('../services/utilities');
const AuthHandler = require('../services/feature/AuthHandler');
const MailHandler = require('../services/core/MailHandler');

//models
const { User, statuses } = require("../models/User");

module.exports = class AuthController
{
	async register(req, res)
	{
	    //building user object
	    let user = new User({
		    email: req.body.email,
		    password: req.body.password,
		    first_name: req.body.first_name,
		    last_name: req.body.last_name,
			phone_number: req.body.phone_number,
			address: {
				postal_code: req.body.postal_code,
				address_line_1: req.body.address_line_1,
				address_line_2: req.body.address_line_2 == '' ? null : req.body.address_line_2,
				city: req.body.city,
				country: req.body.country == '' ? null : req.body.country
			},
		    roles: ["USER"]
	    });

	    //validation
	    user
		    .validate()
		    .then(async () => {

				//generating tokens
				const auth = await user.generateAccessTokens();

				//hashing password
				await user.hashPassword();

				//sending email confirmation
				const data = {
					name: user.first_name,
					email: user.email
				}

				//sending email
				MailHandler.accountCreated(data);

				//saving user and sending response
				user
					.save()
					.then(() => res.status(201).json(AuthHandler.getLoginUser(user, auth)))
					.catch(error => console.log(error));
		    })
		    .catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
	}

	async login (req, res)
	{
		AuthHandler
			.handleLogin(req, User, statuses)
			.then(response => res.json(response))
			.catch(error => res.status(error.code).json(Utils.parseStringError(error.message, error.field)));
	}

	async remindPassword(req, res)
	{
		AuthHandler
			.handlePasswordRemind(req, User)
			.then(() => res.json({ status: true }))
			.catch(error => res.status(error.code).json(Utils.parseStringError(error.message, error.field)));
	}

	async remindPasswordChange(req, res)
	{
		AuthHandler
			.handlePasswordRemindChange(req, User)
			.then(() => res.json({ status: true }))
			.catch(error => res.status(error.code).json(error.error ? Utils.parseValidatorErrors(error.error) : Utils.parseStringError(error.message, error.field)));
	}
}
