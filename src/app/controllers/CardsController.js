
//services
const Utils = require('../services/utilities');
const StripeHandler = require('../services/feature/StripeHandler');

module.exports = class CardsController
{
	async deleteCustomerCard(req, res)
	{
		//checking customer
		if(!req.user.customer)
			return res.status(404).json(Utils.parseStringError('Card not found', 'card'));

		//getting card
		const card = req.user.customer.cards.find(card => card._id.toString() == req.params.id);

		//not found
		if(!card)
			return res.status(404).json(Utils.parseStringError('Card not found', 'card'));

		//deleting from stripe
		const handler = new StripeHandler();
		await handler.deleteCard(req.user.customer, card).catch(error => console.log(error))

		//deleting from local
		req.user.customer.cards.pull(card);
		await req.user.save().catch(error => console.log(error))

		//sending response
		res.json({ status: true })
	}
}
