
//core
const bcrypt = require('bcrypt-nodejs');

//services
const Utils = require('../services/utilities');
const UsersHandler = require("../services/feature/UsersHandler");

//models
const { User } = require("../models/User");

module.exports = class UsersController
{
    profile(req, res)
    {
        const user = {
            _id: req.user._id,
            email: req.user.email,
            first_name: req.user.first_name,
            last_name: req.user.last_name,
			phone_number: req.user.phone_number,
			roles: req.user.roles,
			permissions: req.user.permissions,
			...req.user.address ? {
				address: {
					postal_code: req.user.address.postal_code,
					address_line_1: req.user.address.address_line_1,
					address_line_2: req.user.address.address_line_2,
					city: req.user.address.city,
					country: req.user.address.country
				}
			} : {},
			...req.user.customer ? {
				customer: {
					stripe_id: req.user.customer.stripe_id,
					cards: req.user.customer.cards
				}
			} : {}
        }

        res.json(User.parse(user,req));
    }

    updateProfile(req, res)
    {
        req.user.set({
	        first_name: req.body.first_name || req.user.first_name,
	        last_name: req.body.last_name || req.user.last_name,
			phone_number: req.body.phone_number || req.user.phone_number
        });

        if(req.user.address)
		{
			req.user.address.set({
				postal_code: req.body.postal_code || req.user.address.postal_code,
				address_line_1: req.body.address_line_1 || req.user.address.address_line_1,
				address_line_2: req.body.address_line_2 === '' ? null : req.body.address_line_2 || req.user.address.address_line_2,
				city: req.body.city || req.user.address.city,
				country: req.body.country === '' ? null : req.body.country || req.user.address.country
			})
		}

        //validation, saving user and sending response
        req.user
            .save()
            .then(() => res.json({ status: true }))
	        .catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
    }

    changePassword(req, res)
    {
		UsersHandler
			.handleChangePassword(req)
			.then(() => res.json({ status: true }))
			.catch(error => res.status(error.code).json(error.error ? Utils.parseValidatorErrors(error.error) : Utils.parseStringError(error.message, error.field)));
    }

    changeEmail(req, res)
    {
		const email = req.user.email;
		req.user.email = req.body.email;

		bcrypt.compare(req.body.password, req.user.password, (error, status) => {

			//wrong password
			if (!status)
				return res.status(406).json(Utils.parseStringError("Your password is incorrect", "password"));

			req.user
				.validate()
				.then(() => {

					//sending response
					res.json({ status: true });

					//saving user when email has changed
					if(req.user.email != email)
						req.user.save().catch(error => console.log(error));
				})
				.catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
		});
    }
}

