//core
const mongoose = require("mongoose");

//services
const Utils = require('../services/utilities');
const FileHandler = require("./../services/core/FileHandler");
const ProductsQueryHandler = require("./../services/repositories/ProductsQueryHandler");
const CollectionHandler = require("./../services/feature/CollectionHandler");

//models
const { Category } = require("../models/Category");
const { Product, gameModes: AvailableGameModes, languages: AvailableLanguages } = require("../models/Product");

module.exports = class ProductsController
{
	async addProduct(req, res)
	{
		//generating id
		const id = mongoose.Types.ObjectId();

		// identity verification upload
		const handler = new FileHandler(req, res);
		const filePath = await handler.handleSingleUpload("image", "products/" + id, { allowedMimeTypes: [ "image/jpg", "image/jpeg", "image/png" ], maxFileSize: 10 });

		//getting category
		const category = await Category.findById(req.body.category).exec().catch(error => console.log("Invalid object id"));

		//preparing collections
		const languages = CollectionHandler.prepareCollection(req.body["languages"], AvailableLanguages);
		const gameModes = CollectionHandler.prepareCollection(req.body["game_modes"], AvailableGameModes);

	    //building product object
	    const product = new Product({
			_id: id,
		    title: req.body.title,
		    category: category,
			image: filePath,
			manufacturer: req.body.manufacturer,
			availability: req.body.availability,
			intro: req.body.intro,
			description: req.body.description,
			price: req.body.price,
			age_category: req.body.age_category,
			languages: languages.length ? languages : null,
			game_modes: gameModes.length ? gameModes : null,
			release_type: req.body.release_type,
			premiere: req.body.premiere,
	    });

	    //saving product
		product
		    .save()
			.then(() => res.status(201).json({ status: true }))
		    .catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
	}

	async getProducts(req, res)
	{
		//getting category
		const category = await Category.findById(req.query.category).exec().catch(error => console.log("Invalid object id"));

		//preparing query
		const { pipeline } = ProductsQueryHandler.productsListHandle(req, category);

		// sorts
		const sortConfig = {
			created: "created",
			title: "title",
			manufacturer: "manufacturer",
			price: "price",
			rate: "rate",
			premiere: "premiere",
			availability: "availability",
		};
		const options = Utils.addSortOption({}, req.query.sort, sortConfig);

		// pagination and parsing
		const paginated = await Utils.paginate(Product, { query: Product.aggregate(pipeline), options: options }, req, true);
		paginated.results.map(product => Product.parse(product, req));

		res.json(paginated);
	}

	async getHomeProducts(req, res)
	{
		//preparing query
		const { pipeline } = ProductsQueryHandler.productsListHandle({ query: {}});

		//newest
		const newestPipeline = pipeline.slice();
		newestPipeline.unshift({ $match: { premiere: { $lte: new Date() }} }, { $sort: { premiere: -1, _id: 1 }}, { $limit: 3 });
		const newestProducts = await Product.aggregate(newestPipeline).exec();
		newestProducts.map(product => Product.parse(product, req));


		//premieres
		const premieresPipeline = pipeline.slice();
		premieresPipeline.unshift({ $match: { premiere: { $gte: new Date() }} }, { $sort: { premiere: 1, _id: 1 }}, { $limit: 3 });
		const premieresProducts = await Product.aggregate(premieresPipeline).exec();
		premieresProducts.map(product => Product.parse(product, req));

		//recommended
		const recommendedPipeline = pipeline.slice();
		recommendedPipeline.unshift({ $match: { premiere: { $lte: new Date() }} }, { $sort: { rate: -1, _id: 1 }}, { $limit: 3 });
		const recommendedProducts = await Product.aggregate(recommendedPipeline).exec();
		recommendedProducts.map(product => Product.parse(product, req));

		//sending response
		const response = {
			newest: newestProducts,
			premieres: premieresProducts,
			recommended: recommendedProducts
		}

		res.json(response);
	}

	async getProduct(req, res)
	{
		const product = await Product.findById(req.params.id, {
			title: 1,
			category: 1,
			image: 1,
			manufacturer: 1,
			availability: 1,
			intro: 1,
			description: 1,
			price: 1,
			age_category: 1,
			languages: 1,
			game_modes: 1,
			release_type: 1,
			premiere: 1,
			rate: 1,
			created: 1
		})
		.populate({
			path: 'category',
			select: { title: 1, created: 1 }
		})
		.lean()
		.exec()
		.catch(error => console.log("Invalid object id"));

		// not found
		if (!product)
			return res.status(404).json(Utils.parseStringError('Product not found', 'product'));

		//sending response
		res.json(Product.parse(product, req));
	}

	async updateProduct(req, res)
	{
		const product = await Product.findById(req.params.id).exec().catch(error => console.log("Invalid object id"));

		// not found
		if (!product)
			return res.status(404).json(Utils.parseStringError('Product not found', 'product'));

		// identity verification upload
		const handler = new FileHandler(req, res);
		const filePath = await handler.handleSingleUpload("image", "products/" + product._id, { allowedMimeTypes: [ "image/jpg", "image/jpeg", "image/png" ], maxFileSize: 10, fileToRemove: product.image });

		//preparing collections
		const languages = CollectionHandler.prepareCollection(req.body["languages"], AvailableLanguages);
		const gameModes = CollectionHandler.prepareCollection(req.body["game_modes"], AvailableGameModes);

		product.set({
			title: req.body.title || product.title,
			category: (await Category.findById(req.body.category).exec().catch(error => console.log("Invalid object id"))) || product.category,
			image: filePath || product.image,
			manufacturer: req.body.manufacturer || product.manufacturer,
			availability: req.body.availability || product.availability,
			intro: req.body.intro || product.intro,
			description: req.body.description || product.description,
			price: req.body.price || product.price,
			age_category: req.body.age_category || product.age_category,
			languages: languages.length ? languages : product.languages,
			game_modes: gameModes.length ? gameModes : product.game_modes,
			release_type: req.body.release_type || product.release_type,
			premiere: req.body.premiere || product.premiere
		})

		// saving product
		product
			.save()
			.then(() => res.json({ status: true }))
			.catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
	}

	async uploadPhoto(req, res)
	{
		//photo upload
		const handler = new FileHandler(req, res);
		const filePath = await handler.handleSingleUpload("photo", "products/photos", { allowedMimeTypes: [ "image/jpg", "image/jpeg", "image/png" ], appendTimestamp: true });

		res.status(201).json({
			status: true,
			originalName: req.file ? req.file.originalname : null,
			generatedName: req.file ? req.file.filename : null,
			msg: "Image upload successful",
			imageUrl: handler.getFileUrl(filePath)
		})
	}

	async deleteProduct(req, res)
	{
		const product = await Product.findById(req.params.id).exec().catch(error => console.log("Invalid object id"));

		// not found
		if (!product)
			return res.status(404).json(Utils.parseStringError('Product not found', 'product'));

		//removing product
		await product.remove().catch(error => console.log(error));

		//sending response
		res.json({ status: true })
	}
}
