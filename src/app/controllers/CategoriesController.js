
//services
const Utils = require('../services/utilities');
const CategoriesQueryHandler = require("./../services/repositories/CategoriesQueryHandler");

//models
const { Category } = require("../models/Category");
const { Product } = require("../models/Product");

module.exports = class CategoriesController
{
	async addCategory(req, res)
	{
		//checking parent category
		let parent = null;
		if(req.body.parent)
			parent = await Category.findById(req.body.parent).exec().catch(error => console.log("Invalid object id"));

	    //building category object
	    const category = new Category({
		    title: req.body.title,
		    parent: parent
	    });

	    //saving category
		category
		    .save()
			.then(() => res.status(201).json({ status: true }))
		    .catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
	}

	async getCategories(req, res)
	{
		//checking parent category
		let parent = null;
		if(req.query.parent)
			parent = await Category.findById(req.query.parent).exec().catch(error => console.log("Invalid object id"));

		const { pipeline } = CategoriesQueryHandler.categoriesListHandle(req);
		if(req.query.parent)
			pipeline.unshift({ $match: { parent: parent ? parent._id : null }})

		// sorts
		const sortConfig = {
			created: "created",
			title: "title",
			parent: "parent.title"
		};
		const options = Utils.addSortOption({}, req.query.sort, sortConfig);

		// pagination and parsing
		const paginated = await Utils.paginate(Category, { query: Category.aggregate(pipeline), options: options }, req, true);
		paginated.results.map(category => Category.parse(category, req));

		res.json(paginated);
	}

	async getCategory(req, res)
	{
		const category = await Category.findById(req.params.id, {
			title: 1,
			parent: 1,
			created: 1
		})
		.populate({
			path: 'parent',
			select: { title: 1, created: 1 }
		})
		.lean()
		.exec()
		.catch(error => console.log("Invalid object id"));

		// not found
		if (!category)
			return res.status(404).json(Utils.parseStringError('Category not found', 'category'));

		//sending response
		res.json(Category.parse(category, req));
	}

	async updateCategory(req, res)
	{
		const category = await Category.findById(req.params.id).exec().catch(error => console.log("Invalid object id"));

		// not found
		if (!category)
			return res.status(404).json(Utils.parseStringError('Category not found', 'category'));

		category.set({
			title: req.body.title || category.title,
			parent: req.body.parent == "" ? null : (await Category.findById(req.body.parent).exec().catch(error => console.log("Invalid object id"))) || category.parent
		})

		// saving category
		category
			.save()
			.then(() => res.json({ status: true }))
			.catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
	}

	async deleteCategory(req, res)
	{
		const category = await Category.findById(req.params.id).exec().catch(error => console.log("Invalid object id"));

		// not found
		if (!category)
			return res.status(404).json(Utils.parseStringError('Category not found', 'category'));

		//checking products associations
		if(await Product.count({ category: category }).exec())
			return res.status(409).json(Utils.parseStringError('Cannot delete category associated to products', 'product'));

		//removing category
		await category.remove().catch(error => console.log(error));

		//sending response
		res.json({ status: true })
	}
}
