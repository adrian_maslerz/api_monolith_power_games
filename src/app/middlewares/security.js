/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//custom
const Utils = require('../services/utilities');
const SecurityHandler = require('../services/core/SecurityHandler');

module.exports = function (requiredPermissions = [])
{
    return function (req, res, next)
    {
	    if(!SecurityHandler.verifyPermissions(req.user, requiredPermissions))
		    return res.status(403).json(Utils.parseStringError("Permission Denied", "permission"));

	    next();
    }
}

    
        

    
