/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//models
const { User, statuses } = require("../models/User");

//services
const Utils = require('../services/utilities');
const SecurityHandler = require('../services/core/SecurityHandler');

module.exports = function(req, res, next)
{
    //getting token
    const token = req.headers['x-access-token'] || req.query["access-token"] || req.body["access-token"];

    //auth verification
    SecurityHandler
        .authenticate(token, User, statuses)
        .then(user => {
            req.user = user;
            next();
        })
        .catch(error => res.status(error.status).json(Utils.parseStringError(error.error, error.field)));
}

    
        

    
