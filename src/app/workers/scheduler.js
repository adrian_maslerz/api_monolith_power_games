//loading config
require('dotenv').config();
require("./../../config/database").load();

//core
const cron = require('node-cron');

//models
const { Order } = require("./../models/Order");
const { ProductCopy } = require("./../models/ProductCopy"); //required to ensure product model is registered

//services
const MailHandler = require("./../services/core/MailHandler")

cron.schedule('* * * * *', async () => {

    //orders to send
    const orders = await Order.find({ "items.sent": false }).populate("items.product").exec().catch(error => console.log(error));
    const now = new Date();
    await Promise.all(orders.map(order => {
        return new Promise(async resolve => {
            await Promise.all(order.items.map(item => {
                return new Promise(async resolve => {
                    if(!item.sent)
                    {
                        if(item.product && item.product.premiere.getTime() < now.getTime())
                        {
                            item.sent = true;

                            //sending email
                            const data = {
                                email: order.user_details.email,
                                name: order.user_details.first_name,
                                codes: item.codes
                            }
                            MailHandler.gameCodes(data);
                        }
                    }
                    resolve();
                });
            }))

            //saving model if modified
            if(order.isModified())
                await order.save().catch(error => console.log(error));

            resolve();
        });
    }));
})
