//services
const AppRouter = require('../app/services/core/AppRouter');

//controllers
const AuthController = require('../app/controllers/AuthController');
const CardsController = require("../app/controllers/CardsController");
const CategoriesController = require("./../app/controllers/CategoriesController");
const GeneralController = require('../app/controllers/GeneralController');
const ProductsController = require("./../app/controllers/ProductsController");
const OrdersController = require("../app/controllers/OrdersController");
const UsersController = require('../app/controllers/UsersController');

//middlewares
const authenticate = require('../app/middlewares/authenticate');
const security = require('../app/middlewares/security');

const path = "/v1";
const routes = [

	//Auth
	{
		path: "/", controller: AuthController, children: [
			{ path: "/register", method: "post", function: "register" },
			{ path: "/login", method: "post", function: "login" },
			{ path: "/password/remind", method: "post", function: "remindPassword" },
			{ path: "/password/remind", method: "put", function: "remindPasswordChange" }
		]
	},

	//Cards
	{
		path: "/cards", controller: CardsController, middlewares: [ authenticate, security(["USER_FULL"]) ], children: [
			{ path: "/:id", method: "delete", function: "deleteCustomerCard" },
		]
	},

	//Categories
	{
		path: "/categories", controller: CategoriesController, children: [
			{ path: "/", method: "get", function: "getCategories" },
			{ path: "/:id", method: "get", function: "getCategory" },
			{ path: "/", method: "post", middlewares: [ authenticate, security(["ADMIN_FULL"]) ], function: "addCategory" },
			{ path: "/:id", method: "put", middlewares: [ authenticate, security(["ADMIN_FULL"]) ], function: "updateCategory" },
			{ path: "/:id", method: "delete", middlewares: [ authenticate, security(["ADMIN_FULL"]) ], function: "deleteCategory" },
		]
	},

	//General
	{
		path: "/contact", controller: GeneralController, method: "post", function: "contact"
	},

	//Orders
	{
		path: "/orders", controller: OrdersController, children: [
			{ path: "/", method: "post", middlewares: [ authenticate, security(["USER_FULL"]) ], function: "addOrder" },
			{ path: "/", method: "get", middlewares: [ authenticate ], function: "getOrders" },
			{ path: "/reviews", method: "get", function: "getReviewsHome" },
			{ path: "/:id", method: "get", middlewares: [ authenticate ], function: "getOrder" },
			{ path: "/:order/items/:id", method: "post", middlewares: [ authenticate, security(["USER_FULL"]) ], function: "reviewOrderProduct" },
			{ path: "/:order/items/:id", method: "put", middlewares: [ authenticate, security(["ADMIN_FULL"]) ], function: "changeReviewState" }
		]
	},

	//Products
	{
		path: "/products", controller: ProductsController, children: [
			{ path: "/", method: "get", function: "getProducts" },
			{ path: "/", method: "post", middlewares: [ authenticate, security(["ADMIN_FULL"]) ], function: "addProduct" },
			{ path: "/home", method: "get", function: "getHomeProducts" },
			{ path: "/:id", method: "get", function: "getProduct" },
			{ path: "/:id", method: "put", middlewares: [ authenticate, security(["ADMIN_FULL"]) ], function: "updateProduct" },
			{ path: "/:id", method: "delete", middlewares: [ authenticate, security(["ADMIN_FULL"]) ], function: "deleteProduct" },
			{ path: "/photos", method: "post", middlewares: [ authenticate, security(["ADMIN_FULL"]) ], function: "uploadPhoto" }
		]
	},

	//User
	{
		path: "/user", controller: UsersController, middlewares:[ authenticate ], children: [
			{ path: "/profile", method: "get", function: "profile"},
			{ path: "/profile", method: "put", function: "updateProfile"},
			{ path: "/password", method: "put", function: "changePassword"},
			{ path: "/email", method: "put", function: "changeEmail"}
		]
	}
];

module.exports.load = function(app)
{
	//create routes
	let appRouter = new AppRouter();
	appRouter.create(routes);

	//documentation
	appRouter.registerDocumentation(path);

	app.use(path, appRouter.router);
}


